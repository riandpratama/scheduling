<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use App\Models\KirimCoin;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Mail\EmailNotifKirimChip;
use App\Mail\EmailSuccessKirimChip;

class KirimCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        return view('kirim-coin.index', compact('coin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $this->validate($request, [
            'coin'  => 'required|numeric',
            'biaya' => 'required',
            'password' => ['required', new MatchOldPassword],
        ]);

        $userCheck = User::where('id', $uuid)->first();
        
        if (is_null($userCheck)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();
            $now = Carbon::now()->format('Y-m-d');
            $user = User::with('userdetail')->with('kirimcoinpenjual')->with('ultracoin')->where('id', $uuid)->first();
            $userPenerima = User::with('userdetail')->with('kirimcoin')->with('ultracoin')->where('username', $request->username)->first();

            if ($request->coin == 0){
                Session::flash('warning', 'Maaf Tidak dapat mengirim chip!');
                return redirect()->back();
            }

            if (is_null($userPenerima)){
                Session::flash('warning', 'Username tidak tersedia, harap masukkan dengan benar!');
                return redirect()->back();
            }
            
            if ($userPenerima->username == Auth::user()->username){
                Session::flash('warning', 'Tidak dapat mengirimkankan ke username nya sendiri!');
                return redirect()->back();
            }

            if ($user->ultracoin->coin < $coin->coin){
                Session::flash('warning', 'Mohon maaf ada kesalahan, saldo chip anda kurang untuk melakukan penjualan!');
            } else {
                $noLastId = DB::table('kirim_coins')->max('faktur');
                if (is_null($noLastId)){
                    $substr = 1;
                } else {
                    $substr = substr($noLastId, -1)+1;
                }
                $date   = Carbon::now()->format('Ymd');
                $time   = Carbon::now()->format('his');
                $faktur = $date.'/'.$time.'/'.$substr;
                
                $kirim = KirimCoin::with('kirimcoin')->with('kirimcoinpenjual')->create([
                    'user_id_pembeli' => $userPenerima->id,
                    'user_id_penjual' => $user->id,
                    'ultra_coin_id' => $user->ultracoin->id,
                    'faktur' => $faktur,
                    'coin' => $request->coin,
                    'coin_saat_ini' => $user->ultracoin->coin,
                    'coin_berkurang' => ($user->ultracoin->coin - $request->coin),
                    'biaya' => str_replace(',', '', str_replace('.', '',$request->biaya)) ?: 0,
                    'isActive' => 1
                ]);

                if ($userPenerima->ultracoin->tgl_mining == $now){
                    if (is_null($userPenerima->ultracoin->topup)){
                        $userPenerima->ultracoin->update([
                            'topup'  => $request->coin,
                            'upload_pembayaran' => 'kirim-chip'
                        ]);
                    } else {
                        $userPenerima->ultracoin->update([
                            'topup'  => ($userPenerima->ultracoin->topup + $request->coin),
                            'upload_pembayaran' => 'kirim-chip'
                        ]);
                    }
                    
                } else {
                    $userPenerima->ultracoin->create([
                        'user_id' => $userPenerima->id,
                        'tgl_mining' => $now,
                        'coin' => $userPenerima->ultracoin->coin,
                        'isActive' => 1,
                        'topup' => $request->coin,
                        'upload_pembayaran' => 'kirim-chip'
                    ]);
                }

                $user->ultracoin->update([
                    'coin' => ($user->ultracoin->coin - $request->coin)
                ]);

                Mail::to($userPenerima->email)->send(new EmailNotifKirimChip($kirim));
                Mail::to($user->email)->send(new EmailSuccessKirimChip($kirim));

                Session::flash('success', 'Berhasil, anda telah mengirim chip ke pada penerima dan otomatis chip anda akan berkurang.');
            }
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
