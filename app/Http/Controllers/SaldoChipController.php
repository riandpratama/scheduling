<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use Illuminate\Http\Request;

class SaldoChipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'asc')->get();
        $saldo = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        // $end = new Carbon('first day of last month');
        // $sekarang = Carbon::now()->format('d');
        // $bulanterakhir = Carbon::now()->endOfMonth()->format('d');
        // $kuranghari = Carbon::now()->endOfMonth()->format('d') - date('d', strtotime($saldo->tgl_mining));
        // dd($sekarang,$bulanterakhir, $kuranghari, ($bulanterakhir/$sekarang)*100);

        return view('saldo.index', compact('data', 'saldo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
