<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\KonfirmasiTopup;

class TopUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topup = KonfirmasiTopup::with('user')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->where('isActive', 1)->first();
        $topupverify = KonfirmasiTopup::with('user')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->where('isActive', 2)->first();

        return view('topup.index', compact('topup', 'topupverify'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {   
        $this->validate($request, [
            'coin'  => 'required|numeric',
            'biaya' => 'required'
        ]);

        $user = User::where('id', $uuid)->first();
        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            $noLastId = DB::table('konfirmasi_topups')->max('faktur');
            
            if (is_null($noLastId)){
                $substr = 1;
            } else {
                $substr = substr($noLastId, -1)+1;
            }

            $date   = Carbon::now()->format('Ymd');
            $time   = Carbon::now()->format('his');
            $faktur = $date.'/'.$time.'/'.$substr;

            KonfirmasiTopup::create([
                'user_id'       => Auth::user()->id,
                'ultra_coin_id' => $user->ultracoin->id,
                'faktur'        => $faktur,
                'coin'          => $request->coin,
                'biaya'         => substr(str_replace(',', '', $request->biaya), 3),
                'isActive'      => 1
            ]);

            Session::flash('success', 'Berhasil, silahkan konfirmasi pembayaran di menu Konfirmasi Topup.');
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
