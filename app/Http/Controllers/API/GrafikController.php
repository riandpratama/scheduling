<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GrafikController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  uuid  $userId
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        $tahunSekarang = Carbon::now()->format('Y');

        $data = DB::table('ultra_coins')
        ->select(
            DB::raw("DATE_FORMAT(tgl_mining, '%d-%b-%y') as tanggal"),
            DB::raw("SUM(coin) as coin")
        )
        ->where('user_id', $userId)
        ->where('isActive', 1)
        ->groupBy(['tanggal'])
        ->get();

        $array = null;
        foreach ($data as $item => $val) {
            $array[] = $val->tanggal;
            $array[] = $val->coin;
        }

        if (is_Null($array)){
            $chunk = [];
        } else {
            $chunk = array_chunk($array,2);
        }
        
        return response()->json($chunk, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function title()
    {
        $data =  [
            [
                "name"=> "Time",
                "type" => "date",
                "format" => "%d-%b-%y"
            ],
            [
                "name" => "Jumlah Chip",
                "type" => "number",
            ]
        ];

        return response()->json($data,200);
    }
}
