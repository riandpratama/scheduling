<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\BeliCoin;
use App\Models\AmbilCoin;
use App\Models\KirimCoin;
use App\Models\KonfirmasiTopup;
use Illuminate\Http\Request;

class RiwayatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topup()
    {
        $data = KonfirmasiTopup::where('user_id', Auth::user()->id)->where('isActive', 3)->orderBy('created_at', 'desc')->get();

        return view('riwayat.topup', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ambilCoin()
    {
        $data = AmbilCoin::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('riwayat.ambil-coin', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function beliCoin()
    {
        $data = BeliCoin::where('user_id_pembeli', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('riwayat.beli-coin', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jualCoin()
    {
        $data = BeliCoin::where('user_id_penjual', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('riwayat.jual-coin', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kirimCoin()
    {
        $data = KirimCoin::where('user_id_penjual', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('riwayat.kirim-coin', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terimaCoin()
    {
        $data = KirimCoin::where('user_id_pembeli', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        
        return view('riwayat.terima-coin', compact('data'));
    }
}
