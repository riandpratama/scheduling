<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use App\Models\KodeVoucher;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class PembuatanVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KodeVoucher::where('user_id_pemberi', Auth::user()->id)->get();

        return view('pembuatan-voucher.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $this->validate($request, [
            'coin'  => 'required|numeric',
            'biaya' => 'required',
            'password' => ['required', new MatchOldPassword],
        ]);
        $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        $user = User::where('id', $uuid)->first();
        
        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            
            KodeVoucher::create([
                'user_id_pemberi' => Auth::user()->id,
                'kode_vouchers' => \Str::random(8),
                'isActive' => 1,
                'coin_saat_ini' => $request->coin_saat_ini,
                'coin_diambil' => $request->coin,
                'coin_berkurang' => $request->coin_saat_ini - $request->coin,
                'biaya' => str_replace('.', '', $request->biaya)
            ]);

            $coin->update(['coin' => ($coin->coin - $request->coin)]);

            Session::flash('success', 'Berhasil, anda dapat menggunakan kode voucher tersebut pada member baru.');
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
