<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $saldo = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();
        $sponsor = DB::table('bonus_sponsors')
                ->select(
                    'user_sponsor',
                    DB::raw("SUM(coin) as coin")
                )
                ->where('user_sponsor', Auth::user()->id)
                ->groupBy('user_sponsor')
                ->first();
        $mining = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();
        $ripening = $mining->topup;
        $temporary = $mining->topup_temporary;
        
        if ( $mining->coin < 10) {
            $mining = 'Tidak Aktif';
        } else {
            $mining = 'Aktif';
        }

        $support = DB::table('deskripsi')->where('keterangan', 'support')->first();
        $informasi = DB::table('deskripsi')->where('keterangan', 'informasi')->first();
        
        return view('home', compact('saldo', 'sponsor', 'mining', 'ripening', 'support', 'informasi', 'temporary'));
    }
}
