<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use App\Models\AmbilCoin;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Mail\EmailNotifJualChip;
use App\Mail\EmailVerifikasiJualCoin;

class AmbilCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        $data = AmbilCoin::where('user_id', Auth::user()->id)->where('isActive', 1)->orderBy('created_at', 'desc')->first();
        // dd($data, Auth::user()->id);
        return view('ambil-coin.index', compact('coin', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $this->validate($request, [
            'coin'  => 'required',
            'biaya' => 'required',
            'password' => ['required', new MatchOldPassword],
        ]);

        $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        $userCheck = User::where('id', $uuid)->first();

        if ($request->coin == 0){
            Session::flash('warning', 'Maaf Tidak dapat mengirim chip!');
            return redirect()->back();
        }

        if (is_null($userCheck)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            $user = AmbilCoin::create([
                'user_id'               => Auth::user()->id,
                'ultracoin_id'          => $coin->id,
                'tgl_mining_terakhir'   => $coin->tgl_mining,
                'coin_saat_ini'         => $coin->coin,
                'coin_diambil'          => $request->coin,
                'coin_berkurang'        => ($coin->coin - $request->coin),
                'biaya'                 => str_replace(',', '', str_replace('.', '',$request->biaya)) ?: 0,
                'isActive'              => 1
            ]);
            
            Mail::to($userCheck->email)->send(new EmailNotifJualChip($user));
            Mail::to(config('app.email'))->send(new EmailVerifikasiJualCoin($user));

            Session::flash('success', 'Berhasil, anda dapat melihat riwayat penukaran, dan menghubungi administrator.');
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
