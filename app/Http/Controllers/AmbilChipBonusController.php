<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Models\UltraCoin;
use App\Models\BonusSponsor;
use Illuminate\Http\Request;

class AmbilChipBonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BonusSponsor::with('user')
                ->where('user_sponsor', Auth::user()->id)
                ->orderBy('created_at', 'desc')
                ->get();

        $totalChip = DB::table('bonus_sponsors')
                ->select(DB::raw('SUM(coin) as total'))
                ->where('user_sponsor', Auth::user()->id)
                ->where('isGet', 0)
                ->first();

        return view('ambil-chip-bonus.index', compact('data', 'totalChip'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $ultracoin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();

        $totalChip = DB::table('bonus_sponsors')
                ->select(DB::raw('SUM(coin) as total'))
                ->where('user_sponsor', Auth::user()->id)
                ->where('isGet', 0)
                ->first();
            
        DB::table('bonus_sponsors')
                ->where('user_sponsor', Auth::user()->id)->where('isGet', 0)
                ->update(['isGet' => 1]);

        if (is_null($totalChip->total)) {
            $totalChip = 0;
        } else {
            $totalChip = $totalChip->total;
        }

        $ultracoin->update([
            'topup'  => ($ultracoin->topup + $totalChip),
            'upload_pembayaran' => 'bonus-chip'
        ]);

        Session::flash('success', 'Ambil Bonus Chip berhasil!');
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
