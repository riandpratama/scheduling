<?php

namespace App\Http\Controllers\Store;

use DB;
use Auth;
use Session;
use App\Models\Pemesanan;
use App\Models\WbzStockroom;
use Illuminate\Http\Request;
use App\Models\PemesananDetail;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    public function curl()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: bdc2c9178690de70fad0d5ec4707f40b"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        return json_decode($response, true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wbz = WbzStockroom::where('user_id', Auth::user()->id)->where('status', 1)->first();
        
        if (!count(\Cart::getContent()) > 0) {
            return redirect()->route('store.list');
        }

        $data = $this->curl();

        return view('store.checkout-product.index', compact('data', 'wbz'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'nohp' => 'required',
            'provinsi_id' => 'required',
            'kabupaten_id' => 'required',
            'alamat' => 'required',
        ]);

        $noLastId = DB::table('toko_pemesanans')->max('id');
        $tambah1 = $noLastId+1;

        $date = \Carbon\Carbon::now()->format('Ymd');
        $tgl = \Carbon\Carbon::now()->format('h');

        //Kode pemesanan
        $kodepemesanan = '#' . $date . '-' . $tgl . $tambah1;
        
        $user = Auth::user();

        $data = Pemesanan::create([
            'kode_pemesanan' => $kodepemesanan,
            'user_id_pembeli' => $user->id,
            'user_id_penjual' => '01537fd0-4698-4fc6-8429-51b1e58bee94',
            'provinsi_id' => $request->provinsi_id,
            'provinsi' => $request->provinsi,
            'kabupaten_id' => $request->kabupaten_id,
            'kabupaten' => $request->kabupaten,
            'alamat_kirim' => $request->alamat,
            'total_pesan' => $request->total_barang,
            'harga_coin' => $request->total_pesan,
            'harga_rupiah' => ($request->total_pesan*20000),
            'status' => 0,
        ]);

        foreach ($request->get('produk_id') as $index => $val){
            $detailpemesanan = PemesananDetail::create([
                'pemesanan_id' => $data->id,
                'produk_id' => $request->produk_id[$index],
                'jumlah_produk' => $request->jumlah_barang[$index],
                'harga_total_produk' => $request->harga_total_barang[$index],
                'harga_total_produk_rupiah' => $request->harga_total_produk_rupiah[$index],
                'harga_produk' => $request->harga_produk[$index],
                'harga_produk_rupiah' => $request->harga_produk_rupiah[$index],
            ]);

        }

        \Cart::session(Auth::user()->id)->isEmpty();

        Session::flash('success_message', ' Invoice.');

        return redirect()->route('checkout.invoice',[$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoice($id)
    {
        $data = Pemesanan::findOrFail($id);

        return view('store.invoice.index', compact('data'));
    }
}
