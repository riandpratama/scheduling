<?php

namespace App\Http\Controllers\Store;

use Auth;
use App\Models\Pemesanan;
use Illuminate\Http\Request;
use App\Models\PemesananDetail;
use App\Models\WbzStockroomRiwayat;
use App\Http\Controllers\Controller;

class RiwayatStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = WbzStockroomRiwayat::where('user_id', Auth::user()->id)->where('keterangan', 'tukar-saldo')->orderBy('created_at', 'desc')->get();

        return view('store.riwayat.tukar-saldo', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chip()
    {
        $data = WbzStockroomRiwayat::where('user_id', Auth::user()->id)->where('keterangan', 'ambil-saldo')->orderBy('created_at', 'desc')->get();

        return view('store.riwayat.tukar-saldo-chip', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kirim()
    {
        $data = WbzStockroomRiwayat::where('user_id', Auth::user()->id)->where('keterangan', 'kirim-saldo')->orderBy('created_at', 'desc')->get();

        return view('store.riwayat.kirim-saldo', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terima()
    {
        $data = WbzStockroomRiwayat::where('user_id', Auth::user()->id)->where('keterangan', 'kirim-saldo-member')->orderBy('created_at', 'desc')->get();

        return view('store.riwayat.terima-saldo', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pemesanan()
    {
        $data = Pemesanan::with(['pemesanandetail' => function($query){
            $query->with('produk');
        }])->where('user_id_pembeli', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('store.riwayat.pemesanan', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
