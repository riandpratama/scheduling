<?php

namespace App\Http\Controllers\Store;

use Auth;
use Mail;
use Session;
use App\User;
use App\Models\UltraCoin;
use Illuminate\Http\Request;
use App\Models\WbzStockroom;
use App\Rules\MatchOldPassword;
use App\Mail\EmailStoreTopupWbz;
use App\Models\WbzStockroomRiwayat;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class SaldoWBZController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coin = UltraCoin::where('user_id', Auth::user()->id)->orderBy('tgl_mining', 'desc')->first();

        $wbz = WbzStockroom::where('user_id', Auth::user()->id)->where('status', 1)->first();

        $wbzRiwayat = WbzStockroomRiwayat::where('user_id', Auth::user()->id)->where('keterangan', 'tukar-saldo')->where('status', 0)->orderBy('created_at', 'desc')->first();

        return view('store.change-saldo.index', compact('coin', 'wbz', 'wbzRiwayat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $this->validate($request, [
            'wbz'  => 'required',
            'biaya' => 'required',
            'password' => ['required', new MatchOldPassword],
        ]);

        $userCheck = User::where('id', $uuid)->first();
        
        if (is_null($userCheck)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            if ($request->biaya < 20000){
                Session::flash('warning', 'Maaf tidak dapat menukar chip wbz!');
                return redirect()->back();
            } else {
                $wbz = WbzStockroom::where('user_id', Auth::user()->id)->where('status', 1)->first();

                if (is_null($wbz)) {
                    $wbzCreate = WbzStockroom::create([
                        'user_id' => Auth::user()->id,
                        'wbz' => $request->wbz,
                        'status' => 0,
                    ]);
                    $riwayat = WbzStockroomRiwayat::create([
                        'user_id' => Auth::user()->id,
                        'wbz_stockroom_id' => $wbzCreate->id,
                        'wbz' => $request->wbz,
                        'rupiah' => $request->biaya,
                        'status' => 0,
                        'keterangan' => 'tukar-saldo'
                    ]);
                } else {
                    $riwayat = WbzStockroomRiwayat::create([
                        'user_id' => Auth::user()->id,
                        'wbz_stockroom_id' => $wbz->id,
                        'wbz' => $request->wbz,
                        'rupiah' => $request->biaya,
                        'status' => 0,
                        'keterangan' => 'tukar-saldo'
                    ]);
                }

                Mail::to(config('app.email'))->send(new EmailStoreTopupWbz($riwayat));
                Mail::to($userCheck->email)->send(new EmailStoreTopupWbz($riwayat));

                Session::flash('success', 'Berhasil, anda telah menukarkan chip wbz menjadi wbz stockroom, tunggu Admin memverifikasi penukaran Anda.');
            }
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
