<?php

namespace App\Http\Controllers\Store;

use DB;
use Auth;
use App\Models\TokoProduk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('toko_produks')->orderBy('created_at', 'desc')->get();

        $message = DB::table('toko_messages')->where('to', Auth::id())->orderBy('created_at', 'desc')->first();
        
        return view('store.list-product.index', compact('data', 'message'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string  $search
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {   
        $name = $request->search;

        $data = DB::table('toko_produks')->where('slug', 'like', '%'.$name.'%')->get();
        
        $message = DB::table('toko_messages')->where('to', Auth::id())->orderBy('created_at', 'desc')->first();

        return view('store.list-product.index', compact('data', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = TokoProduk::with('produkimage')->where('slug', $slug)->first();

        return view('store.detail-product.index', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
