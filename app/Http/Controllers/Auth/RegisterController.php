<?php

namespace App\Http\Controllers\Auth;

use DB;
use Mail;
use Session;
use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Mail\EmailVerification;
use App\Notofications\MailUserVerification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * redirect to create register page.
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function store(Request $request)
    {
        $check = DB::table('sponsors')->where('nama_sponsor', $request->nama_sponsor)->first();
        Session::flash('nama_sponsor', $request->nama_sponsor);
        Session::flash('email', $request->email);
        Session::flash('username', $request->username);

        if (!is_null($check)) {
            $request->validate([
                'nama_sponsor' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            
            try {
                $user = User::create([
                    'id' => Str::uuid(),
                    'username' => $request->username,
                    'id_sponsor' => $check->user_id,
                    'nama_sponsor' => $request->nama_sponsor,
                    'email' => $request->email,
                    'password' => Hash::make($request->password)
                ]);

                Mail::to($request->email)->send(new EmailVerification($user));

                Session::flash('Success', 'Registrasi Berhasil, Mohon verifikasi melalui email yang telah anda daftarkan!');
                return redirect()->to('/login');
            } catch (Exception $e) {
                return redirect()->back();
            }
        } else {
            Session::flash('Danger', 'Mohon Maaf, ID Sponsor anda tidak tesedia!');
            return redirect()->to('register');
        }
    }

    public function userActivation($uuid)
    {
        $check = DB::table('users')->where('id', $uuid)->first();

        if (!is_null($check)) {
            $user = User::findOrFail($check->id);
            if (!is_null($user->email_verified_at)) {
                Session::flash('Info', 'Akun anda sudah Aktif!');
                return redirect()->to('login');
            }
            $user->update(['email_verified_at' => Date::now()]);
            Session::flash('Success', 'Akun Terverifikasi, Anda dapat login');
            return redirect()->to('login');
        }
        Session::flash('Danger', 'Mohon Maaf, Akun anda tidak tesedia!');
        return redirect()->to('login');
    }
}
