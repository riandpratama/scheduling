<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Session;
use App\User;
use App\Models\{
    Sponsor,
    UltraCoin,
    SewaCloud,
    UserDetails,
    KodeVoucher,
    BonusSponsor,
    RegistrasiAkun
};
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\EmailRegistrationAdmin;

class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('activation.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_lengkap'      => 'required',
            'no_ktp'            => 'required',
            'tgl_lahir'         => 'required',
            'alamat'            => 'required',
            'no_hp'             => 'required',
            'nama_bank'         => 'required',
            'nama_pemilik_rekening' => 'required',
            'nama_cabang'       => 'required',
            'no_rekening'       => 'required',
            'swift_code'        => 'required',
            'kode_voucher'      => 'required',
            'upload_foto'       => 'required',
            'upload_ktp'        => 'required',
            'upload_pembayaran' => 'required',
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $kodeVoucher = KodeVoucher::where('kode_vouchers', $request->kode_voucher)
                ->where('isActive',1)
                ->orderBy('created_at', 'desc')
                ->first();
        
        if (!is_null($kodeVoucher)) {

            try {
                if ($request->hasFile('upload_foto')) {
                    $foto = $request->file('upload_foto')->store('uploads/foto');
                } else {
                    $foto = NULL;
                }

                if ($request->hasFile('upload_ktp')) {
                    $ktp = $request->file('upload_ktp')->store('uploads/ktp');
                } else {
                    $ktp = NULL;
                }

                if ($request->hasFile('upload_pembayaran')) {
                    $pembayaran = $request->file('upload_pembayaran')->store('uploads/pembayaran');
                } else {
                    $pembayaran = NULL;
                }

                $userdetail = UserDetails::create([
                    'user_id' => Auth::user()->id,
                    'nama_lengkap' => $request->nama_lengkap,
                    'no_ktp' => $request->no_ktp,
                    'tgl_lahir' => $request->tgl_lahir,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'no_hp' => $request->no_hp,
                    'nama_bank' => $request->nama_bank,
                    'nama_pemilik_rekening' => $request->nama_pemilik_rekening,
                    'nama_cabang' => $request->nama_cabang,
                    'no_rekening' => $request->no_rekening,
                    'swift_code' => $request->swift_code,
                    'upload_foto' => $foto,
                    'upload_ktp' => $ktp,
                ]);
                Sponsor::create([
                    'user_id' => Auth::user()->id,
                    'nama_sponsor' => Auth::user()->username,
                ]);
                UltraCoin::create([
                    'user_id' => Auth::user()->id,
                    // 'coin' => 40,
                    'upload_pembayaran' => $pembayaran,
                    // 'tgl_mining' => Carbon::now()->format('yy-m-d'),
                    'ripening' => 1
                ]);
                BonusSponsor::create([
                    'user_id' => Auth::user()->id,
                    'user_sponsor' => Auth::user()->id_sponsor,
                    'coin' => 4,
                    'tanggal_transaksi' => Carbon::now()->format('yy-m-d'),
                ]);
                RegistrasiAkun::create([
                    'user_id' => Auth::user()->id,
                    'biaya' => 50000
                ]);
                $kodeVoucher->update([
                    'user_id' => Auth::user()->id,
                    'isActive' => 0
                ]);
                $user->update([
                    'isRegistration' => 1,
                ]);

                Mail::to(config('app.email'))->send(new EmailRegistrationAdmin($user));

            } catch (Exception $e) {
                return redirect()->back();
            }
        } else {
            Session::flash('Danger', "Mohon Maaf, Kode Voucher $request->kode_voucher tidak tesedia!");
            return redirect()->back();
        }

        Session::flash('Success', "Berhasil, Tunggu pihak Administrator memverifikasi akun anda.");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexverify()
    {
        return view('activation.index-verify');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
