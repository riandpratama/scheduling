<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\KonfirmasiTopup;
use Illuminate\Http\Request;

use App\Mail\EmailKonfirmasiTopup;
use App\Mail\EmailKonfirmasiTopupAdmin;

class KonfirmasiTopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KonfirmasiTopup::where('user_id', Auth::user()->id)->where('isActive', '!=' , 3)->orderBy('created_at', 'desc')->get();

        return view('konfirmasi-topup.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid, $id)
    {
        $user = KonfirmasiTopup::with('user')->where('user_id', $uuid)->where('id', $id)->first();

        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            if ($request->hasFile('upload_bukti')) {
                $foto = $request->file('upload_bukti')->store('uploads/bukti-topup');
            } else {
                $foto = NULL;
            }

            $user->update([
                'isActive'      => 2,
                'upload_bukti'  => $foto
            ]);

            Mail::to($user->user->email)->send(new EmailKonfirmasiTopup($user));
            Mail::to('support@outlookbarbershop.com')->send(new EmailKonfirmasiTopupAdmin($user));

            Session::flash('success', 'Berhasil, tunggu pihak administrator memverifikasi.');
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
