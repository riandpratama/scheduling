<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\BeliCoin;
use Illuminate\Http\Request;
use App\Mail\EmailBeliCoin;
use App\Mail\EmailNotifikasiBeliCoin;

class BeliCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beli = BeliCoin::with('user')->where('user_id_pembeli', Auth::user()->id)->orderBy('created_at', 'desc')->where('isActive', 0)->first();
        
        return view('beli-coin.index', compact('beli'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid)
    {
        $this->validate($request, [
            'coin'          => 'required|numeric',
            'biaya'         => 'required',
            'upload_bukti'  => 'required'
        ]);

        $user = User::where('id', $uuid)->first();
        
        $penjual = User::where('username', $request->penjual)->first();

        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {

            if (is_null($penjual)) {
                Session::flash('warning', 'Mohon maaf ada kesalahan, Username Member Pemberi Chip yang anda inputkan salah!');
            } else {
                
                if ($request->hasFile('upload_bukti')) {
                    $foto = $request->file('upload_bukti')->store('uploads/bukti-beli');
                } else {
                    $foto = NULL;
                }
                
                BeliCoin::create([
                    'user_id_pembeli'   => Auth::user()->id,
                    'user_id_penjual'   => $penjual->id,
                    'coin'              => $request->coin,
                    'biaya'             => $request->biaya,
                    'isActive'          => 0,
                    'upload_bukti'      => $foto
                ]);

                Mail::to($user->email)->send(new EmailBeliCoin($user));
                Mail::to($penjual->email)->send(new EmailNotifikasiBeliCoin($user));

                Session::flash('success', 'Berhasil, tunggu konfirmasi member memverifikasi pembelian chip anda.');
            }
            
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
