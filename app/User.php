<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Webpatser\Uuid\Uuid;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'email_verified_at', 'username', 'id_sponsor', 'nama_sponsor', 'isActive', 'tanggal_berakhir_cloud', 'isRegistration'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function userdetail()
    {
        return $this->hasOne('App\Models\UserDetails', 'user_id');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function sponsor()
    {
        return $this->hasOne('App\Models\Sponsor', 'user_id');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function ultracoin()
    {
        return $this->hasOne('App\Models\UltraCoin', 'user_id')->orderBy('created_at', 'desc');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function topup()
    {
        return $this->hasOne('App\Models\KonfirmasiTopup', 'user_id')->orderBy('created_at', 'desc');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function kirimcoin()
    {
        return $this->hasOne('App\Models\KirimCoin', 'user_id_pembeli')->orderBy('created_at', 'desc');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function kirimcoinpenjual()
    {
        return $this->hasOne('App\Models\KirimCoin', 'user_id_penjual')->orderBy('created_at', 'desc');
    }
}
