<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'toko_messages';
    
    protected $guarded = [];

    public function userfrom()
    {
    	return $this->belongsTo('App\User', 'from');
    }

    public function userto()
    {
    	return $this->belongsTo('App\User', 'to');
    }
}