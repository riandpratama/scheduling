<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokoProduk extends Model
{
    protected $table = 'toko_produks';

    protected $guarded = [];

    public function produkimage()
    {
        return $this->hasMany(TokoProdukImage::class, 'produk_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}