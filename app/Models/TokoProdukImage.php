<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokoProdukImage extends Model
{
    protected $table = 'toko_produk_images';

    protected $guarded = [];

    public function produkimage()
    {
        return $this->hasMany(ProdukImage::class, 'produk_id');
    }
}