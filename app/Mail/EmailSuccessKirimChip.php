<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSuccessKirimChip extends Mailable
{
    use Queueable, SerializesModels;

    protected $kirim;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($kirim)
    {

        $this->kirim = $kirim;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.success-kirim-chip')
                ->with(['user' => $this->kirim])
                ->from(config('app.email'));
    }
}
