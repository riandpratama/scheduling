<?php

namespace App\Mail;

use App\Models\KirimCoin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotifKirimChip extends Mailable
{
    use Queueable, SerializesModels;

    protected $kirim;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(KirimCoin $kirim)
    {

        $this->kirim = $kirim;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notif-kirim-chip')
                ->with(['kirim' => $this->kirim])
                ->from(config('app.email'));
    }
}
