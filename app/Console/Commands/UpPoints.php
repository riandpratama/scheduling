<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use App\User;
use App\Models\UltraCoin;

class UpPoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The command increase point everyday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coin = UltraCoin::where('coin', '>=', 10)
                ->where('isActive', '=', 1)
                ->whereDate('tgl_mining', \Carbon\Carbon::yesterday())
                ->get();
        
        $checkRipening = UltraCoin::where('ripening', '=', 0)
                ->whereDate('tgl_mining', \Carbon\Carbon::yesterday())
                ->get();

        $array = null;
        foreach ($coin as $item){
            $array[] = $item->user_id;
            $array[] = $item->coin;
            $array[] = $item->tgl_mining;
            $array[] = $item->ripening;
            $array[] = $item->topup;
        }
        // dd($array);
        $array_chunk = array_chunk($array, 5);
        // dd($array_chunk);
        foreach ($array_chunk as $key => $val){

            if ($array_chunk[$key][3] == 0){
                // if ($array_chunk[$key][5] == 0) {
                    $coinUp = ($array_chunk[$key][1]*0.2/100) + $array_chunk[$key][1] + $array_chunk[$key][4];
                // } else {
                //  $coinUp = ($array_chunk[$key][1]*0.2/100)+$array_chunk[$key][1];
                // }
            } else {
                $coinUp = $array_chunk[$key][1];
            }

            UltraCoin::create([
                'user_id' => $array_chunk[$key][0],
                'coin' => $coinUp,
                'tgl_mining' => \Carbon\Carbon::now()->format('Y-m-d'),
                'ripening' => $array_chunk[$key][3] > 0 ? $array_chunk[$key][3] - 1 : 0 ,
                // 'is_topup' => $array_chunk[$key][5] > 0 ? $array_chunk[$key][5] - 1 : 0 ,
                'topup' =>  $array_chunk[$key][4],
                'isActive' => 1
            ]);
        }
    }
}
