<?php
Route::get('/cache', function(){
	Artisan::call('config:cache');
});

Route::get('/', function(){
	return view('frontend.index');
})->name('index');

Route::get('/kebijakan', function(){
	return view('frontend.kebijakan');
})->name('kebijakan');

Route::get('mailable', function () {
    $user = \App\User::with('userdetail')->where('id', '3128d742-c2a5-4aa8-b99c-a893e828af28')->first();
    return new App\Mail\EmailRegistrationAdmin($user);
});

Route::group(['namespace' => 'Service'], function() {
	Route::get('/cek-kabupaten', 'RajaOngkirController@cekKabupaten');
	Route::post('/cek-ongkir', 'RajaOngkirController@cekOngkir');
});

Route::group(['namespace' => 'Auth'], function(){
	Route::get('register', 'RegisterController@index')->name('register');
	Route::post('register', 'RegisterController@store')->name('register.create');

	Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
	Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
	Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');

	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');

	Route::get('verify/user/{id}', 'RegisterController@userActivation')->name('verify.user');

	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@postLogin');
	Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth'], function(){
	Route::group(['middleware' => 'verified'], function(){

		Route::group(['middleware' => 'step-registration'], function(){
			Route::get('step-registration', 'ActivationController@index')->name('step-registration');
			Route::post('step-registration/store', 'ActivationController@store')->name('step-registration-store');
		});

		Route::get('/verify-account', 'ActivationController@indexverify')->name('userverify');
		Route::group(['middleware' => 'active'], function(){
			Route::get('home', 'HomeController@index')->name('home');
			Route::get('saldo-chip', 'SaldoChipController@index')->name('saldo');
			Route::get('profile', 'ProfileController@index')->name('profile');
			Route::post('profile/update/{uuid}/{id}', 'ProfileController@update')->name('profile.update');

			Route::get('topup', 'TopUpController@index')->name('topup');
			Route::post('topup/store/{uuid}', 'TopUpController@store')->name('topup.store');
			Route::get('konfirmasi-topup', 'KonfirmasiTopupController@index')->name('topup');
			Route::post('konfirmasi-topup/store/{uuid}/{id}/update', 'KonfirmasiTopupController@store')->name('store');

			Route::get('ambil-coin', 'AmbilCoinController@index')->name('ambil-coin');
			Route::post('ambil-coin/store/{uuid}', 'AmbilCoinController@store')->name('ambil-coin.store');

			Route::get('pembuatan-voucher', 'PembuatanVoucherController@index')->name('pembuatan-voucher');
			Route::post('pembuatan-voucher/store/{uuid}', 'PembuatanVoucherController@store')->name('pembuatan-voucher.store');

			Route::get('beli-chip', 'BeliCoinController@index')->name('beli-coin');
			Route::post('beli-chip/store/{uuid}', 'BeliCoinController@store')->name('beli-coin.store');

			Route::get('kirim-chip', 'KirimCoinController@index')->name('kirim-coin');
			Route::post('kirim-chip/store/{uuid}', 'KirimCoinController@store')->name('kirim-coin.store');

			Route::get('ambil-chip-bonus', 'AmbilChipBonusController@index')->name('ambil.coin.bonus');
			Route::post('ambil-chip-bonus/store/{uuid}/update', 'AmbilChipBonusController@store')->name('ambil.coin.bonus.store');

			Route::get('konfirmasi-jual-chip', 'KonfirmasiJualCoinController@index')->name('jual-coin');
			Route::post('konfirmasi-jual-chip/store/{uuid}/{id}', 'KonfirmasiJualCoinController@store')->name('jual-coin.store');
			Route::post('konfirmasi-jual-chip/batal/{uuid}/{id}', 'KonfirmasiJualCoinController@batal')->name('jual-coin.batal');

			Route::get('ubah-password', 'UbahPasswordController@index')->name('ubah-password');
			Route::post('ubah-password/store', 'UbahPasswordController@store')->name('ubah-password.store');

			Route::get('riwayat-topup', 'RiwayatController@topup');
			Route::get('riwayat-ambil-coin', 'RiwayatController@ambilCoin');
			Route::get('riwayat-beli-chip', 'RiwayatController@beliCoin');
			Route::get('riwayat-jual-chip', 'RiwayatController@jualCoin');
			Route::get('riwayat-kirim-chip', 'RiwayatController@kirimCoin');
			Route::get('riwayat-terima-chip', 'RiwayatController@terimaCoin');

			Route::get('faq', 'FAQController@index')->name('faq');
			Route::get('layanan-cs', 'LayananCSController@index')->name('layanan-cs');

			Route::group(['namespace' => 'Store'], function() {
				Route::get('riwayat-tukar-saldo-wbz', 'RiwayatStoreController@index')->name('wbz.list');
				Route::get('riwayat-tukar-saldo-wbz-chip', 'RiwayatStoreController@chip')->name('chip.list');
				Route::get('riwayat-kirim-saldo-wbz', 'RiwayatStoreController@kirim')->name('kirim.list');
				Route::get('riwayat-terima-saldo-wbz', 'RiwayatStoreController@terima')->name('terima.list');
				Route::get('riwayat-pemesanan', 'RiwayatStoreController@pemesanan')->name('pemesanan.list');

				Route::get('saldo-wbz', 'SaldoWBZController@index')->name('wbz.list');
				Route::post('saldo-wbz/store/{uuid}', 'SaldoWBZController@store')->name('wbz.store');

				Route::get('saldo-chip-wbz', 'SaldoChipController@index')->name('wbz.chip.list');
				Route::post('saldo-chip-wbz/store/{uui}', 'SaldoChipController@store')->name('wbz.chip.store');

				Route::get('kirim-wbz', 'KirimSaldoWBZController@index')->name('wbz.kirim.list');
				Route::post('kirim-wbz/store/{uui}', 'KirimSaldoWBZController@store')->name('wbz.kirim.store');

				Route::get('store-penjualan', 'ListProductController@index')->name('store.list');
				Route::get('store-penjualan-search', 'ListProductController@search')->name('store.search');
				Route::get('store-penjualan/show/{slug}', 'ListProductController@show')->name('store.show');

				Route::get('store-penjualan/cart', 'CartController@index')->name('cart.index');
				Route::post('store-penjualan/cart', 'CartController@store')->name('cart.store');
				Route::patch('store-penjualan/{product}', 'CartController@update')->name('cart.update');
				Route::delete('store-penjualan/{product}', 'CartController@destroy')->name('cart.destroy');

				Route::get('store-penjualan/checkout', 'CheckoutController@index')->name('checkout.index');
				Route::post('store-penjualan/checkout/store', 'CheckoutController@store')->name('checkout.store');

				Route::get('invoice/{id}', 'CheckoutController@invoice')->name('checkout.invoice');

				Route::get('message/{user_id}', 'MessageController@index')->name('message');
				Route::post('message/{user_id}/store', 'MessageController@store')->name('message.store');
			});
		});
	});
});