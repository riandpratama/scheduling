<p style="text-align: center;">Verifikasi Member</p>

<p> Hi Administrator, harap melakukan verifikasi pada member dibawah ini: </p>

<p> Email Member: {{ $user->email }}</p>
<p> Usernane Member: {{ $user->username }}</p>
<p> Sponsor Dari: {{ $user->nama_sponsor }}</p>
<p> Tanggal Registrasi: {{ date('d/m/Y', strtotime($user->created_at)) }}</p>

<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>

