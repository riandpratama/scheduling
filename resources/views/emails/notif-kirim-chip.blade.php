Hi, {{ $kirim->kirimcoin->userdetail->nama_lengkap }} (#{{ $kirim->kirimcoin->username }}) <br>
Kirim Chip Informasi: <br>
<br>
Faktur: {{ $kirim->faktur }} <br>
Nilai Chip: {{ $kirim->coin }} <br>
Nilai Rupiah: Rp {{ number_format($kirim->biaya, 2, ',', '.') }} <br>
Dikirim dari: {{ Auth::user()->email }} (#{{ Auth::user()->username }}) <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($kirim->created_at)) }}<br>
<br>
Transaksi Berhasil <br>
<br>
Informasi Bank Transfer:<br>
<br>
Bank: {{ $kirim->kirimcoin->userdetail->nama_bank }}<br>
Cabang: {{ $kirim->kirimcoin->userdetail->nama_cabang }}<br>
Swift Code: {{ $kirim->kirimcoin->userdetail->swift_code }}<br>
No Akun Bank: {{ $kirim->kirimcoin->userdetail->no_rekening }}<br>
Nama Pemilik Akun: {{ $kirim->kirimcoin->userdetail->nama_pemilik_rekening }}<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>