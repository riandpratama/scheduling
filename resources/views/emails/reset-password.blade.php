<style type="text/css">
    .button {
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      padding: 3px 5px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      border-radius: 10px;
    }
</style>


<p style="text-align: center;">Reset Password</p>

<p> Anda menerima email ini karena kami menerima permintaan pengaturan ulang kata sandi untuk akun Anda. </p>

<p style="color:gray;"> Administrator akan memproses perubahan password anda. </p>

<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
