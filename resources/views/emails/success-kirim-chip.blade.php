Hi, {{ $user->kirimcoinpenjual->userdetail->nama_lengkap }} (#{{ $user->kirimcoinpenjual->username }}) <br>
Kirim Chip Informasi: <br>
<br>
Faktur: {{ $user->faktur }} <br>
Nilai Chip: {{ $user->coin }} <br>
Nilai Rupiah: Rp {{ number_format($user->biaya, 2, ',', '.') }} <br>
Dikirim ke: {{ $user->kirimcoin->email }} (#{{ $user->kirimcoin->username }}) <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($user->created_at)) }}<br>
<br>
Transaksi Berhasil <br>
<br>
Informasi Bank Transfer:<br>
<br>
Bank: {{ $user->kirimcoinpenjual->userdetail->nama_bank }}<br>
Cabang: {{ $user->kirimcoinpenjual->userdetail->nama_cabang }}<br>
Swift Code: {{ $user->kirimcoinpenjual->userdetail->swift_code }}<br>
No Akun Bank: {{ $user->kirimcoinpenjual->userdetail->no_rekening }}<br>
Nama Pemilik Akun: {{ $user->kirimcoinpenjual->userdetail->nama_pemilik_rekening }}<br>
<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>