Hi, {{ $riwayat->user->userdetail->nama_lengkap }} (#{{ $riwayat->user->username }}) <br>
Tukar Chip Wbz to Stockroom Wbz Informasi: <br>
<br>
Nilai Wbzstockroom: {{ $riwayat->wbz }} <br>
Nilai Rupiah: Rp {{ number_format($riwayat->rupiah, 2, ',', '.') }} <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($riwayat->created_at)) }}<br>
<br>
Menunggu Verifikasi Administrator <br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>