Hi, {{ $user->user->userdetail->nama_lengkap }} (#{{ $user->user->username }}) <br>
Top up Informasi: <br>
<br>
Faktur: {{ $user->faktur }} <br>
Nilai Chip: {{ $user->coin }} <br>
Nilai Rupiah: Rp {{ number_format($user->biaya, 2, ',', '.') }} <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($user->created_at)) }}<br>
<br>
Menunggu Verifikasi Administrator
<br>
<br>
<br>
Informasi Bank Transfer:<br>
<br>
Bank: {{ $user->user->userdetail->nama_bank }}<br>
Cabang: {{ $user->user->userdetail->nama_cabang }}<br>
Swift Code: {{ $user->user->userdetail->swift_code }}<br>
No Akun Bank: {{ $user->user->userdetail->no_rekening }}<br>
Nama Pemilik Akun: {{ $user->user->userdetail->nama_pemilik_rekening }}<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
