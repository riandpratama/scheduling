<style type="text/css">
    .button {
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      padding: 3px 5px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      border-radius: 10px;
    }
</style>


<p style="text-align: center;">Verifikasi Email Kamu</p>

<p> Hi {{ $user->email }}, hanya beberapa langkah lagi sebelum kamu dapat melanjutkan ke pendaftaran. </p>

<p style="color:gray;"> Gunakan tombol dibawah ini untuk memverifikasi email kamu. </p>

<div class="button">
    <a href="{{ url('verify/user', $user->id) }}" class="button" target="_blank">Verifikasi Email Sekarang</a>
</div>

<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
