<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Step Pendaftaran Akun</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <style>
		* {
	    	margin: 0;
	    	padding: 0
		}

		html {
		    height: 100%
		}

		p {
		    color: grey
		}

		#heading {
		    text-transform: uppercase;
		    color: black;
		    font-weight: normal
		}

		#msform {
		    text-align: center;
		    position: relative;
		    margin-top: 20px
		}

		#msform fieldset {
		    background: white;
		    border: 0 none;
		    border-radius: 0.5rem;
		    box-sizing: border-box;
		    width: 100%;
		    margin: 0;
		    padding-bottom: 20px;
		    position: relative
		}

		.form-card {
		    text-align: left
		}

		#msform fieldset:not(:first-of-type) {
		    display: none
		}

		#msform input,
		#msform textarea {
		    padding: 8px 15px 8px 15px;
		    border: 1px solid #ccc;
		    border-radius: 0px;
		    margin-bottom: 25px;
		    margin-top: 2px;
		    width: 100%;
		    box-sizing: border-box;
		    font-family: montserrat;
		    color: #2C3E50;
		    background-color: #ECEFF1;
		    font-size: 16px;
		    letter-spacing: 1px
		}

		#msform input:focus,
		#msform textarea:focus {
		    -moz-box-shadow: none !important;
		    -webkit-box-shadow: none !important;
		    box-shadow: none !important;
		    border: 1px solid #00a65a;
		    outline-width: 0
		}

		#msform .action-button {
		    width: 100px;
		    background: #00a65a;
		    font-weight: bold;
		    color: white;
		    border: 0 none;
		    border-radius: 0px;
		    cursor: pointer;
		    padding: 10px 5px;
		    margin: 10px 0px 10px 5px;
		    float: right
		}

		#msform .action-button:hover,
		#msform .action-button:focus {
		    background-color: #311B92
		}

		#msform .action-button-previous {
		    width: 100px;
		    background: #616161;
		    font-weight: bold;
		    color: white;
		    border: 0 none;
		    border-radius: 0px;
		    cursor: pointer;
		    padding: 10px 5px;
		    margin: 10px 5px 10px 0px;
		    float: right
		}

		#msform .action-button-previous:hover,
		#msform .action-button-previous:focus {
		    background-color: #000000
		}

		.card {
		    z-index: 0;
		    border: none;
		    position: relative
		}

		.fs-title {
		    font-size: 25px;
		    color: #00a65a;
		    margin-bottom: 15px;
		    font-weight: normal;
		    text-align: left
		}

		.purple-text {
		    color: #00a65a;
		    font-weight: normal
		}

		.steps {
		    font-size: 25px;
		    color: gray;
		    margin-bottom: 10px;
		    font-weight: normal;
		    text-align: right
		}

		.fieldlabels {
		    color: gray;
		    text-align: left
		}

		#progressbar {
		    margin-bottom: 30px;
		    overflow: hidden;
		    color: lightgrey
		}

		#progressbar .active {
		    color: #00a65a
		}

		#progressbar li {
		    list-style-type: none;
		    font-size: 15px;
		    width: 33%;
		    float: left;
		    position: relative;
		    font-weight: 400
		}

		#progressbar #account:before {
		    font-family: FontAwesome;
		    content: "\f13e"
		}

		#progressbar #personal:before {
		    font-family: FontAwesome;
		    content: "\f007"
		}

		#progressbar #payment:before {
		    font-family: FontAwesome;
		    content: "\f030"
		}

		#progressbar #confirm:before {
		    font-family: FontAwesome;
		    content: "\f00c"
		}

		#progressbar li:before {
		    width: 50px;
		    height: 50px;
		    line-height: 45px;
		    display: block;
		    font-size: 20px;
		    color: #ffffff;
		    background: lightgray;
		    border-radius: 50%;
		    margin: 0 auto 10px auto;
		    padding: 2px
		}

		#progressbar li:after {
		    content: '';
		    width: 100%;
		    height: 2px;
		    background: lightgray;
		    position: absolute;
		    left: 0;
		    top: 25px;
		    z-index: -1
		}

		#progressbar li.active:before,
		#progressbar li.active:after {
		    background: #00a65a
		}

		.progress {
		    height: 20px
		}

		.progress-bar {
		    background-color: #00a65a
		}

		.fit-image {
		    width: 100%;
		    object-fit: cover
		}
		#select {
			padding: 8px 15px 8px 15px;
		    border: 1px solid #ccc;
		    border-radius: 0px;
		    margin-bottom: 25px;
		    margin-top: 2px;
		    width: 100%;
		    box-sizing: border-box;
		    font-family: montserrat;
		    color: #2C3E50;
		    background-color: #ECEFF1;
		    font-size: 16px;
		    letter-spacing: 1px;
		}
	</style>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid">
    <div class="row justify-content-center">
    	@if (Auth::user()->isRegistration == 0)
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
            <h3 >Halaman Pendaftaran Member Baru</h3>
            <p>Input semua form dan klik next step</p>
            <form id="msform" method="POST" action="{{ route('step-registration-store') }}" enctype="multipart/form-data">
            	{{ csrf_field() }}
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active" id="personal"><strong>Data Identitas</strong></li>
                    <li id="payment"><strong>Data Upload</strong></li>
                    <li id="account"><strong>Konfirmasi</strong></li>
                </ul>
                <div class="progress">
                    <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                </div> <br> <!-- fieldsets -->
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Data Identitas:</h2>
                            </div>
                        </div> 

                        @if (Session::has('Danger'))
				            <div class="alert alert-danger">
				                {{ Session::get('Danger') }}
				            </div>
				        @endif

                        <label class="fieldlabels">Username: </label> 
                        <input type="text" readonly="" placeholder="{{ Auth::user()->username }}" /> 

                        <label class="fieldlabels">Email: </label> 
                        <input type="text" readonly="" placeholder="{{ Auth::user()->email }}" /> 
						
                        <label class="fieldlabels">Nama Lengkap <i style="color:red;">*</i></label> 
                        @if ($errors->has('nama_lengkap'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="nama_lengkap" value="{{ old('nama_lengkap') }}" /> 

                        <label class="fieldlabels">No KTP: <i style="color:red;">*</i></label>
                        @if ($errors->has('no_ktp'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif 
                        <input type="number" name="no_ktp" value="{{ old('no_ktp') }}"/>

                        <label class="fieldlabels">Tanggal Lahir: <i style="color:red;">*</i></label> 
                        @if ($errors->has('tgl_lahir'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif 
                        <input type="date" name="tgl_lahir" value="{{ old('tgl_lahir') }}" /> 

                        <label class="fieldlabels">Jenis Kelamin: <i style="color:red;">*</i></label> 
                        
                        <select name="jenis_kelamin" class="form-control" id="select">
                        	<option value="Laki-Laki" @if (old('jenis_kelamin') == 'Laki-Laki') selected @endif>Laki-Laki</option>
                        	<option value="Perempuan" @if (old('jenis_kelamin') == 'Perempuan') selected @endif>Perempuan</option>
                        </select>

                        <label class="fieldlabels">Alamat: <i style="color:red;">*</i></label> 
                        @if ($errors->has('alamat'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="alamat" value="{{ old('alamat') }}" />

                        <label class="fieldlabels">No Handphone: <i style="color:red;">*</i></label> 
                        @if ($errors->has('no_hp'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="no_hp" value="{{ old('no_hp') }}" />

                        <label class="fieldlabels">Nama Bank: <i style="color:red;">*</i></label> 
                        @if ($errors->has('nama_bank'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="nama_bank" value="{{ old('nama_bank') }}" /> 

                        <label class="fieldlabels">Nama Pemilik Rekening: <i style="color:red;">*</i></label> 
                        @if ($errors->has('nama_pemilik_rekening'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="nama_pemilik_rekening" value="{{ old('nama_pemilik_rekening') }}" /> 

                        <label class="fieldlabels">Cabang Bank: <i style="color:red;">*</i></label> 
                        @if ($errors->has('nama_cabang'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="nama_cabang" value="{{ old('nama_cabang') }}" /> 

                        <label class="fieldlabels">No Rekening: <i style="color:red;">*</i></label> 
                        @if ($errors->has('no_rekening'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="no_rekening" value="{{ old('no_rekening') }}" />

                        <label class="fieldlabels">Swift Code Bank: <i style="color:red;">*</i></label> 
                        @if ($errors->has('swift_code'))
	                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
	                    @endif
                        <input type="text" name="swift_code" value="{{ old('swift_code') }}" />
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Next" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Personal Information:</h2>
                            </div>
                        </div> 
						
						<div>
							<label class="fieldlabels">Upload Foto Diri:<i style="color:red;">*</i></label> 
	                        <input type="file" name="upload_foto" id="foto" accept="image/*"> 
	        				<img id="foto_diri" src="#" alt="your image" style="margin-bottom: 20px" />
	        				@if ($errors->has('upload_foto'))
		                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
		                    @endif
	        			</div>
						
						<div>
	                        <label class="fieldlabels">Upload Foto KTP:<i style="color:red;">*</i></label> 
	                        <input type="file" name="upload_ktp" id="ktp" accept="image/*">
	                        <img id="foto_ktp" src="#" alt="your image" style="margin-bottom: 20px" />
	                        @if ($errors->has('upload_ktp'))
		                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
		                    @endif
                    	</div>
						
						<div>
                        	<label class="fieldlabels">Upload Bukti Pembayaran:<i style="color:red;">*</i></label> 
                        	<input type="file" name="upload_pembayaran" id="pembayaran" accept="image/*">
                        	<img id="foto_pembayaran" src="#" alt="your image" style="margin-bottom: 20px" />
                        	@if ($errors->has('upload_pembayaran'))
		                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
		                    @endif
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Konfirmasi Vocher:</h2>
                            </div>
                        </div> 
                        <div>
							<label class="fieldlabels">Kode Voucher: <i style="color:red;">*</i></label> 
	                        @if ($errors->has('kode_voucher'))
		                        <span style="color:red; margin-top: -10px"><strong>Harus di isi!</strong></span>
		                    @endif
	                        <input type="text" name="kode_voucher" value="{{ old('kode_voucher') }}" />
	        			</div>
                    </div> 
                    <button type="submit" class="next action-button" onclick="return confirm('Anda yakin ingin submit?')">Submit</button>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                </fieldset>
                
            </form>
            <div class="">
                <a class="btn btn-primary" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> <b> {{ __('LOGOUT') }} </b>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        @else
        	<div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
	            <h2 id="heading">Mohon tunggu,</h2>
	            <p>Akun anda sedang diproses oleh Administrator</p>

	            <div class="">
	                <a class="btn btn-primary" href="{{ route('logout') }}"
	                   onclick="event.preventDefault();
	                                 document.getElementById('logout-form').submit();">
	                    <i class="fa fa-sign-out"></i> <b> {{ __('LOGOUT') }} </b>
	                </a>

	                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                    @csrf
	                </form>
	            </div>
	        </div>
        @endif
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){

	var current_fs, next_fs, previous_fs; //fieldsets
	var opacity;
	var current = 1;
	var steps = $("fieldset").length;

	setProgressBar(current);

	$(".next").click(function(){

		current_fs = $(this).parent();
		next_fs = $(this).parent().next();

		//Add Class Active
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now) {
			// for making fielset appear animation
				opacity = 1 - now;

				current_fs.css({
					'display': 'none',
					'position': 'relative'
				});
				next_fs.css({'opacity': opacity});
			},
			duration: 500
		});

		setProgressBar(++current);
	});

	$(".previous").click(function(){

		current_fs = $(this).parent();
		previous_fs = $(this).parent().prev();

		//Remove class active
		$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

		//show the previous fieldset
		previous_fs.show();

		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now) {
				// for making fielset appear animation
				opacity = 1 - now;

				current_fs.css({
					'display': 'none',
					'position': 'relative'
				});
				previous_fs.css({'opacity': opacity});
			},
			duration: 500
		});
		setProgressBar(--current);
	});

	function setProgressBar(curStep){
		var percent = parseFloat(100 / steps) * curStep;
			percent = percent.toFixed();
		$(".progress-bar")
			.css("width",percent+"%")
		}

		$(".submit").click(function(){
			return false;
		})

	});

	$('input[type=file]').change(function (e) {
        if(this.files[0].size > 2000000){
           alert("File melebihi kapasitas upload 2 Mb, Mohon Upload file di bawah 2 Mb.");
           this.value = "";
        };
    })

	function readURL() {
		$('#foto_diri').attr('src', '').hide();
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			$(reader).load(function(e) {
				$('#foto_diri').attr('src', e.target.result)
			});
			reader.readAsDataURL(this.files[0]);
		}
	}
	$('#foto_diri')
		.load(function(e) {
			$(this).css('height', '200px').show();
		})
		.hide();
	$("#foto").change(readURL);

	function readURLKTP() {
		$('#foto_ktp').attr('src', '').hide();
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			$(reader).load(function(e) {
				$('#foto_ktp').attr('src', e.target.result)
			});
			reader.readAsDataURL(this.files[0]);
		}
	}
	$('#foto_ktp')
		.load(function(e) {
			$(this).css('height', '200px').show();
		})
		.hide();
	$("#ktp").change(readURLKTP);

	function readURLPembayaran() {
		$('#foto_pembayaran').attr('src', '').hide();
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			$(reader).load(function(e) {
				$('#foto_pembayaran').attr('src', e.target.result)
			});
			reader.readAsDataURL(this.files[0]);
		}
	}
	$('#foto_pembayaran')
		.load(function(e) {
			$(this).css('height', '200px').show();
		})
		.hide();
	$("#pembayaran").change(readURLPembayaran);
</script>

</body>
</html>