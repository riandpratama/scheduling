Syarat-Syarat dan Ketentuan Umum (selanjutnya disebut sebagai “SKU”) WALLBROZ adalah ketentuan yang berisikan syarat dan ketentuan mengenai penggunaan produk, jasa, teknologi, fitur layanan yang diberikan oleh WALLBROZ termasuk, namun tidak terbatas pada penggunaan Website, Dompet WALLBROZ Indonesia dan WALLBROZ Trading Platform (Trading App) (untuk selanjutnya disebut sebagai “Platform WALLBROZ”) sepanjang tidak diatur secara khusus sebagaimana tercantum pada bagian registrasi akun WALLBROZ yang dibuat pada hari dan tanggal yang tercantum dalam bagian registrasi akun https://wallbroz.com, merupakan satu kesatuan tidak terpisahkan dan persetujuan atas SKU ini.Dengan mendaftar menjadi Member/Verified Member, Anda menyatakan telah MEMBACA, MEMAHAMI, MENYETUJUI dan MEMATUHIPersyaratan dan Ketentuan di bawah. Anda disarankan membaca semua persyaratan dan ketentuan secara seksama sebelum menggunakan layanan platform WALLBROZ atau segala layanan yang diberikan, dan bersama dengan ini Anda setuju dan mengikatkan diri terhadap seluruh kegiatan dalam SKU ini dengan persyaratan dan ketentuan sebagai berikut :
<br>
DEFINISI
Sepanjang konteks kalimatnya tidak menentukan lain, istilah atau definisi dalam SKU memiliki arti sebagai berikut :
<br>
<br>
1. Website mengacu pada situs online dengan alamat https://wallbroz.com Website ini dikelola oleh WALLBROZ, dengan tidak terbatas pada para pemilik, investor, karyawan dan pihak-pihak yang terkait dengan WALLBROZ. Tergantung pada konteks, “Website” dapat juga mengacu pada jasa, produk, situs, konten atau layanan lain yang disediakan oleh WALLBROZ.
<br>
<br>
2. Aset Digital adalah komoditas digital yang menggunakan prinsip teknologi desentralisasi berbasiskan jaringan peer-to-peer (antar muka) atau disebut dengan Jaringan Blockchain yang diperdagangkan di dalam platform.
<br>
<br>
3. Blockchain adalah sebuah buku besar terdistribusi (distributed ledger) terbuka yang dapat mencatat transaksi antara dua pihak secara efisien dan dengan cara yang dapat diverifikasi secara permanen.
<br>
<br>
4. Registrasi adalah proses pendaftaran menjadi Member dalam platform WALLBROZ yang merupakan proses verifikasi awal untuk memperoleh keterangan, pernyataan dalam penggunaan layanan platform
<br>
<br>
5. Member adalah orang (perseorangan), badan usaha, maupun badan hukum yang telah melakukan registrasi pada platform WALLBROZ, sehingga memperoleh otorisasi dari platform WALLBROZ untuk melakukan kegiatan perdagangan Aset Digital.
<br>
<br>
6. Verifikasi adalah proses pemeriksaan terhadap member mengenai keterangan terkait member dan informasi pribadi yang dicantumkan dalam proses registrasi untuk divalidasi oleh WALLBROZ guna mendapat layanan penuh platform.
<br>
<br>
7. Verified Member adalah Member yang telah melalui dan menyelesaikan tahapan Verifikasi yang dilakukan oleh WALLBROZ.
<br>
<br>
8. Layanan adalah jasa yang disediakan oleh WALLBROZ dalam memfasilitasi Verified Member untuk melakukan kegiatan membeli dan menjual Aset Digital.
<br>
<br>
9. Kegiatan Perdagangan Aset Digital adalah kegiatan transaksi jual-beli Aset Digital atas dasar adanya pencapaian titik nilai kesepakatan para Verified Member dalam platform yang disediakan oleh WALLBROZ.
<br>
<br>
10. Akun Member adalah akses yang diberikan kepada Member/Verified Member untuk mendapatkan informasi dan untuk melakukan kegiatan perdagangan Aset Digital melalui platform.
<br>
<br>
11. Kata Sandi adalah kumpulan karakter yang terdiri dari rangkaian alfa-numerik atau kombinasi keduanya dan digunakan oleh Member/Verified Member untuk memverifikasi identitas dirinya kepada sistem keamanan pada platform WALLBROZ.
<br>
<br>
12. Nomor Telepon adalah nomor telepon Member/Verified Member yang terdaftar sehingga memperoleh otorisasi untuk mendapatkan layanan. Perubahan nomor telepon dapat dilakukan dengan mengikuti peraturan pada laman bantuan yang tersedia di Website.
<br>
<br>
13. Konversi adalah perubahan nilai mata uang fiat atau kartal ke dalam bentuk atau format Aset Digital berdasarkan nilai tukar/rate yang berlaku dan sebaliknya.
<br>
<br>
14. Pembeli adala hVerified Member yang melakukan pembelian Aset Digital melalui platform di dalam mekanisme transaksinya pembeli dapat melakukan pembelian Aset Digital yang didasarkan dengan nilai tukar/rate mata uang Rupiah.
<br>
<br>
15. Penjual adalah Verified Member yang melakukan penjualan Aset Digital melalui platform di dalam mekanisme transaksinya penjual dapat melakukan penjualan Aset Digital yang didasarkan dengan nilai tukar/rate mata uang Rupiah.
<br>
<br>
16. Deposit adalah proses penyimpanan Rupiah/Aset Digital oleh Member/Verified Member yang merujuk pada proses atau mekanisme penambahan (top up) yang dilakukan melalui Akun Member/Verified Member
<br>
<br>
17. Withdrawal adalah proses penarikan Rupiah/Aset Digital oleh Verified Member yang merujuk pada proses atau mekanisme penarikan (withdrawal) yang dilakukan melalui Akun Verified Member
<br>
<br>
18. Harga Aset Digital adalah nilai tukar/rate Aset Digital dalam platform WALBROZ yang bersifat fluktuatif, dengan nilai sesuai permintaan (supply) dan persediaan (demand) pada market.
<br>
<br>
19. Alamat Aset Digital adalah alamat dompet Aset Digital milik Member/Verified Member, diciptakan khusus untuk Member/Verified Member dan dapat digunakan berkali-kali sebagai dompet Aset Digital yang disediakan di dalam platform Dalam faktor teknisnya, alamat Aset Digital memiliki peranan ataupun fungsi untuk menerima dan mengirim Aset Digital.
<br>
<br>
20. Limit Pengiriman Aset Digital adalah batas minimal dan maksimal dalam melakukan pengiriman Aset Digital ke dompet Aset Digital lain per-harinya. Perlu untuk dijelaskan bahwa setiap Akun Verified Member akan memiliki limit pengiriman yang berbeda dan disesuaikan dengan Syarat dan Ketentuan ini dengan Peraturan Perundang-Undangan yang berlaku.
<br>
<br>
21. LimitPenarikanRupiahadalahbatasminimaldanmaksimalpenarikanRupiahper-harinya. Berkenaan dengan limit penarikan Rupiah. Perlu untuk dijelaskan bahwa setiap Akun Verified Member akan memiliki limit penarikan yang berbeda yang disesuaikan dengan Syarat dan Ketentuan dan Peraturan Perundang-Undangan yang berlaku.
<br>
<br>
22. Rekening Bank adalah rekening bank yang telah didaftarkan oleh Member dengan kewajiban kesamaan nama pemegang rekening dan nama Member.
<br>
<br>
23. Order Book adalah daftar harga jual dan harga beli yang tersedia pada Website. Verified Member dapat membeli atau menjual Aset Digital menggunakan harga yang tertera dan untuk memudahkan Verified Member Order Book dibagi menjadi dua bagian, yaitu :
<br>
<br>
Ø Market Beli – Daftar permintaan pembelian Aset Digital lengkap dengan jumlah Aset Digital dan harga yang ditawarkan.
<br>
<br>
Ø Market Jual – Daftar Aset Digital yang dijual lengkap dengan jumlah Aset Digital dan harga yang diminta.
<br>
<br>
24. Dompet Aset Digital adalah komponen perangkat lunak dan informasi untuk menyimpan dan menggunakan Aset Digital.
<br>
<br>
25. Voucher WALBROZ adalah produk yang dikeluarkan oleh platform WALBROZ berupa bukti kepemilikan Aset Digital dalam bentuk voucher elektronik untuk digunakan dalam kegiatan perdagangan Aset Digital.
<br>
<br>
26. Rupiah adalah mata uang Negara Kesatuan Republik Indonesia. Mata uang yang diperdagangkan terhadap Aset Digital pada platform
<br>
<br>
27. KYC (Know Your Customer) Principles adalah proses penilaian terhadap calon Member dan Member untuk mengetahui latar belakang dan itikad baik terhadap perbuatan yang akan dilakukan dalam sebuah kegiatan perdagangan Aset Digital.
<br>
<br>
28. AML (Anti Money Laundering) adalah kegiatan untuk mengantisipasi dan menghentikan praktik pencucian uang.
<br>
<br>
29. GDPR (General Data Protection Regulation) adalah bentuk instrumen hukum berkenaan dengan Data Privacy yang diterapkan bagi seluruh perusahaan di dunia yang telah menyimpan, mengolah dan memproses data personal para penduduk Uni Eropa. Kebijakan ini memiliki untuk Memberikan perlindungan terhadap kerahasiaan data personal atau perseorangan. Pengaplikasian aturan ini mulai efektif pada tanggal 25 Mei 2018.
<br>
<br>
30. Sistem Keamanan 2 (dua) Langkah (Two Step Verification)adalah layanan yang diberikan platform WALBROZ sebagai opsi bagi Verified Member dalam Memberikan keamanan tambahan atas Akun Verified Member.
<br>
<br>