TANGGUNG JAWAB
<br>
Member/Verified Member setuju untuk menanggung setiap resiko, kerugian atau akibat yang diderita Member/Verified Member yang disebabkan oleh, antara lain :
<br>
<br>
1. Kerusakan, keterlambatan, kehilangan atau kesalahan pengiriman perintah dan komunikasi,
secara elektronik yang disebabkan oleh Member/Verified Member;
<br>
2. Laporan Akun WALLBROZ atau pemberitahuan penggunaan layanan WALLBROZ yang dikirim
kepada Member/Verified Member diterima atau dibaca atau disalahgunakan oleh pihak yang
tidak berwenang atas Akun WALLBROZ,
<br>
3. Password Akun diketahui oleh orang/pihak lain atas kesalahan Member/Verified Member.
Member/Verified Member memahami dan setuju bahwa Member/Verified Member akan menggunakan Akun dan layanan WALLBROZ untuk transaksi yang tidak bertentangan dengan ketentuan peraturan perundang-undangan dan/atau kebijakan internal WALLBROZ yang berlaku dan/atau peraturan-peraturan lainnya yang berlaku secara nasional maupun internasional yang terkait dengan pelaksanaan transaksi tersebut baik secara langsung maupun tidak langsung, dan WALLBROZ tidak akan Memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala klaim dan/atau tuntutan dan/atau kerugian yang timbul sehubungan dengan penggunaan layanan WALLBROZ oleh Member/Verified Member untuk transaksi yang dikategorikan sebagai transaksi yang mencurigakan dan/atau transaksi yang dilarang oleh ketentuan peraturan perundang-undangan dan/atau kebijakan internal WALLBROZ yang berlaku dan/atau peraturan-peraturan lainnya yang berlaku baik secara nasional maupun secara internasional yang terkait dengan kegiatan transaksi yang dilakukan oleh Member/Verified Member secara langsung maupun tidak langsung.
Dalam melakukan transaksi menggunakan layanan WALLBROZ, Member/Verified Member mengerti dan menyetujui bahwa terdapat sanksi-sanksi tertentu yang dikenakan oleh pemerintah Indonesia, termasuk pemerintah Negara lain, dan/atau instansi berwenang lainnya terhadap beberapa negara, badan dan perorangan. Mengacu pada hal tersebut, WALLBROZ berhak untuk tidak melaksanakan/memproses transaksi yang merupakan pelanggaran terhadap ketentuan tersebut, dan instansi berwenang dapat mensyaratkan pengungkapan informasi terkait. WALLBROZ tidak bertanggung jawab apabila wallbroz.com atau pihak lain gagal atau menunda pelaksanaan transaksi, atau mengungkapkan informasi sebagai akibat pelanggaran langsung maupun tidak langsung atas ketentuan peraturan perundang-undangan tersebut.
<br>
<br>
RESIKO<br>
<br>
<br>
1. Perdagangan Aset Digital merupakan aktifitas beresiko tinggi. Harga Aset Digital fluktuatif, di mana harga dapat berubah secara signifikan dari waktu ke waktu. Sehubungan dengan fluktuasi harga, nilai Aset Digital dapat bertambah maupun berkurang secara signifikan sewaktu-waktu. Semua Aset Digital berpotensi untuk mengalami perubahan nilai secara drastis atau bahkan menjadi tidak berarti. Terdapat resiko kehilangan yang tinggi sebagai dampak dari membeli, menjual, atau berdagang apapun di pasar dan WALLBROZ tidak bertanggung jawab atas perubahan fluktuasi dari nilai tukar Aset Digital.
<br>
<br>
2. Perdagangan Aset Digital juga memiliki resiko tambahan yang tidak dialami oleh Aset Digital atau komoditas lain di pasar. Tidak seperti mata uang kebanyakan yang dijamin oleh pemerintah atau lembaga legal lainnya, atau emas dan perak, digital asset merupakan sebuah Aset Digital yang unik dan dijamin oleh teknologi dan rasa percaya. Tidak ada bank sentral yang dapat mengontrol, melindungi nilai Aset Digital dalam krisis, atau mencetak mata uang tersebut.
<br>
<br>
3. Member/Verified Member dihimbau untuk berhati-hati dalam mengukur situasi finansial dan memastikan bahwa Member/Verified Member bersedia menghadapi resiko yang ada dalam menjual, membeli, atau berdagang Aset Digital. Member/Verified Member disarankan dengan sangat untuk melakukan riset pribadi sebelum membuat keputusan untuk memperjual belikan Aset Digital. Semua keputusan perdagangan Aset Digital merupakan keputusan independen oleh pengguna secara sadar tanpa paksaan dan melepaskan WALLBROZ atas akibat dari kegiatan perdagangan Aset Digital.
<br>
<br>
4. WALLBROZ tidak menjamin kelangsungan jangka panjang dari Aset Digital yang diperdagangkan maupun ditukar di dalam marketplace
<br>
<br>
5. BerkenaandengankegiatanpenambanganAsetDigital,perluuntukdiluruskandanditegaskan bahwa WALLBROZ tidak melakukan kegiatan menambang, memproduksi atau mencetak Aset Digital. Aset Digital diciptakan dan ditambang menggunakan software khusus oleh para penambang (miner) yang tersebar secara acak di seluruh dunia dan saling terhubung dengan teknologi peer-to-peer di jaringan blockchain.
<br>
<br>
LARANGAN
<br>
<br>
Tanpa mengurangi maksud dan tujuan dari ketentuan mengenai larangan-larangan yang terdapat dalam SKU, maka Member/Verified Member dilarang untuk melakukan hal-hal sebagai berikut :
<br>
<br>
1. Member/Verified Member tidak diperbolehkan melakukan tindakan-tindakan yang dapat mengakibatkan kerugian bagi WALLBROZ dan/atau yang bertentangan dengan SKU dan peraturan perundang-undangan yang berlaku.
<br>
<br>
2. Apabila Member/Verified Member melanggar salah satu atau beberapa ketentuan dalam ketentuan ini, maka Member/Verified Member akan bertanggung jawab sepenuhnya dan dengan ini setuju bahwa atas pelanggaran tersebut WALLBROZ berhak untuk membatasi fitur penggunaan layanan WALLBROZ dan Member/Verified Member mengetahui dan setuju bahwa WALLBROZ tidak akan Memberikan ganti rugi dan atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala resiko atau akibat yang timbul atas pembatasan penggunaan layanan tersebut. Member/Verified Memberselanjutnya berkewajiban untuk membayar ganti rugi kepada WALLBROZ sebesar nilai kerugian yang mungkin dialami WALLBROZ atas pelanggaran tersebut. Member/Verified Member dengan ini Memberikan kuasa kepada WALLBROZ untuk melakukan pendebetan Akun Member/Verified Member untuk pembayaran ganti rugi tersebut. Dalam hal dana pada Akun Member/Verified Member tidak tersedia dan/atau tidak mencukupi, maka Member wajib mengembalikan seluruh dana tersebut secara tunai kepada WALLBROZ dalam waktu selambat-lambatnya 7 (tujuh) hari kerja sejak WALLBROZ mengajukan klaim yang dimaksud.
<br>
<br>
KERAHASIAAN
<br>
<br>
Selama bekerjasama dengan WALLBROZ dan pada setiap waktu sesudahnya, maka :
<br>
<br>
1. Setiap informasi dan atau data yang diberikan oleh WALLBROZ kepada Member/Verified Member dan atau data yang diperoleh Member/Verified Member sebagai pelaksanaan dari SKU baik yang diberikan atau disampaikan secara lisan, tertulis, grafik atau yang disampaikan melalui media elektronik atau informasi dan/atau data dalam bentuk lainnya selama berlangsungnya pembicaraan atau selama pekerjaan lain adalah bersifat rahasia (selanjutnya disebut sebagai “Informasi Rahasia”).
<br>
<br>
2. Member/Verified Membersetuju dan sepakat bahwa setiap saat akan merahasiakan informasi rahasia yang diperoleh sebagai pelaksanaan dari kerjasama kepada siapapun atau tidak akan menggunakannya untuk kepentingan Member/Verified Member atau kepentingan pihak lainnya, tanpa terlebih dahulu memperoleh persetujuan tertulis dari pejabat yang berwenang dari WALLBROZ atau pihak yang berwenang lainnya sesuai dengan ketentuan hukum yang berlaku.
<br>
<br>
3. Apabila Member/Verified Membermelanggar ketentuan sebagaimana dimaksud dalam angka 1 dan 2 ketentutan mengenai kerahasiaan ini, maka segala kerugian, tuntutan dan atau gugatan yang dialami WALLBROZ merupakan tanggung jawab Member/Verified Member sepenuhnya, dan atas permintaan pertama dari WALLBROZ, Member/Verified Memberberkewajiban untuk menyelesaikannya sesuai dengan ketentuan hukum dan perundang-undangan yang berlaku dan memberikan ganti rugi yang mungkin timbul akibat pelanggaran tersebut kepada WALLBROZ. Member/Verified Member dengan ini setuju bahwa WALLBROZ tidak akan Memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala tuntutan dan atau gugatan yang mungkin timbul dikemudian hari sehubungan dengan pelanggaran tersebut.
<br>
<br>
4. Kewajiban untuk menyimpan informasi rahasia sebagaimana dimaksud dalam angka 1 dan 2 ketentutan mengenai kerahasiaan menjadi tidak berlaku, apabila;
<br>
<br>
a. Informasi rahasia tersebut menjadi tersedia untuk masyarakat umum,<br>
<br>
b. Informasi rahasia tersebut diperintahkan untuk dibuka untuk memenuhi perintah
pengadilan atau badan pemerintah lain yang berwenang,<br>
<br>
c. Informasi rahasia tersebut diberikan sesuai dengan ketentuan hukum yang berlaku.<br>
<br>
5. Member/Verified Member selanjutnya menyetujui untuk melakukan segenap upaya dan mengambil setiap tindakan yang diperlukan untuk menghindari pihak-pihak ketiga dalam memperoleh akses terhadap dan/atau mengakibatkan terjadinya pengungkapan atas informasi rahasia.<br>
<br>
6. Dalam hal kerjasama telah berakhir, Member/Verified Member sepakat bahwa kewajiban untuk merahasiakan dokumen dan materi yang merupakan informasi rahasia sebagaimana diatur dalam ketentuan ini akan tetap berlaku.
<br>
<br>
KELALAIAN<br>
<br>
1. Member/Verified Member sepakat bahwa yang dimaksud dengan Kelalaian (Wanprestasi) adalah hal atau peristiwa sebagai berikut :
<br>
<br>
a. Melakukan ingkar janji atau tidak menepati perjanjian , <br>
<br>
b. Apabila Member/Verified Member lalai dalam melaksanakan suatu kewajiban atau
melanggar ketentuan dalam SKU,<br>
<br>
c. Membuat pernyataan tidak benar,<br>
<br>
d. Bilamana ternyata bahwa dalam suatu pernyataan atau jaminan yang diberikan oleh<br>
<br>
Member/Verified Member tidak benar atau tidak sesuai dengan kenyataannya.
2. Dalam hal terjadi suatu kejadian kelalaian berdasarkan angka 1 ketentuan kelalaian ini oleh Member/Verified Member, maka WALLBROZ dapat memilih apakah tetap meneruskan atau menutup Akun Member/Verified Member. Apabila WALLBROZ berkehendak untuk menutup Akun Member/Verified Member, maka kehendak tersebut harus diberitahukan kepada Member/Verified Member dalam waktu yang wajar menurut WALLBROZ.
<br>
<br>
3. Dalam hal terjadi kejadian kelalaian oleh Member/Verified Membe rsebagaimana dimaksud, maka wallbroz.com berhak dengan seketika melakukan penonaktifan Akun Member/Verified Member pada WALLBROZ tanpa harus melakukan pemberitahuan terlebih dahulu kepada Member/Verified Member, dan Member/Verified Memberdengan ini setuju bahwa WALLBROZ tidak akan Memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Memberatau pihak manapun atas segala tuntutan dan/atau gugatan, klaim, permintaan ganti kerugian dari pihak manapun yang mungkin timbul sehubungan dengan terjadinya kelalaian tersebut.
HAK KEKAYAAN INTELEKTUAL
<br>
<br>
1. Member/Verified Member menyatakan dan menyetujui WALLBROZ sebagai pemegang hak kepemilikan atas layanan, perangkat lunak, teknologi, konten, situs, dan alat pendukung lainnya termasuk dalam Hak Kekayaan Intelektual yang terkait dengan fasilitas WALLBROZ.<br>
<br>
2. Member/Verified Member hanya diperbolehkan untuk melihat, mencetak dan/atau mengunduh salinan material dari Website untuk penggunaan pribadi dan non-komersial. Seluruh penggunaan komersial perlu mendapatkan ijin dari WALLBROZ. Setiap kegiatan komersil tanpa seijin WALLBROZ diartikan sebagai pelanggaran atas Hak Kekayaan Intelektual WALLBROZ dan dapat mengakibatkan pemberhentian Akun WALLBROZ pada Member/Verified Member.
<br>
<br>
PEMBLOKIRAN DAN PEMBEKUAN AKUN MEMBER/ERIFIED MEMBER
<br>
<br>
1. Dalam rangka menjalankan prinsip kehati-hatian, WALLBROZ berhak dan Member/Verified Member dengan ini memberi kuasa kepada WALLBROZ untuk memblokir baik sebagian atau seluruh saldo Member/Verified Member(hold amount) dan/atau mendebetnya serta melakukan pemberhentian Akun, apabila terjadi hal berikut :
<br>
<br>
a. Adanya permintaan dari pihak perbankan dikarenakan adanya kesalahan pengiriman dana dan pihak perbankan tersebut meminta dilakukan pemblokiran;<br>
<br>
b. Menurut pendapat dan pertimbangan WALLBROZ terdapat kesalahan penerimaan transaksi;<br>
<br>
c. WALLBROZ digunakan sebagai tempat penampungan yang diindikasikan sebagai hasil kejahatan termasuk namun tidak terbatas pada tindak pidana korupsi, penyuapan, jual beli narkotika, psikotropika, penyelundupan tenaga kerja, penyelundupan manusia, bidang perbankan, bidang pasar modal, bidang perasuransian, kepabeanan, cukai, perdagangan manusia, perdagangan senjata gelap, terorisme, penculikan, pencurian, penggelapan, penipuan, pemalsuan, perjudian, prostitusi, bidang perpajakan, dan terorisme;
<br>
<br>
d. Member/Verified Member dinilai lalai dalam melakukan kewajiban berdasarkan SKU;
<br>
<br>
e. Kebijakan WALLBROZ atau oleh suatu ketetapan pemerintah atau instansi yang berwenang atau peraturan yang berlaku, sehingga WALLBROZ diharuskan melakukan pemblokiran dan/atau pembekuan Akun WALLBROZ tersebut.
<br>
<br>
2. Apabila terjadi pemberhentian Akun, Member/Verified Member setuju dan menyatakan bersedia untuk tetap terikat dengan SKU menghentikan penggunaan layanan WALLBROZ, memberikan hak kepada wallbroz.com untuk menghapus semua informasi dan data dalam server WALLBROZ, dan menyatakan WALLBROZ tidak bertanggung jawab kepada Member/Verified Member atau Pihak Ketiga atas penghentian akses dan penghapusan informasi serta data dalam Akun Member/Verified Member.
<br>
<br>
PAJAK
<br>
<br>
Bahwa pajak atas aktivitas perdagangan Aset Digital adalah pajak yang ditanggung oleh masing- masing pelaku kegiatan, dalam hal ini Member/Verified Memberdan WALLBROZ. Member/Verified Member dapat mengkonsultasikan pembayaran pajak kepada Konsultan Pajak Pribadi, dan WALLBROZ tidak menanggung pajak Member/Verified Member kecuali ditentukan lain dalam Syarat dan Ketentuan ini. KEAMANAN, WALLBROZ telah menerapkan tindakan kemanan sebagai jaringan pengaman informasi terhadap akses yang tidak sah dalam penggunaan, perubahan dan/atau pengungkapan Akun, dengan standar keamanan yang telah sesuai dengan Ketentuan Peraturan Perundang-undangan yang berlaku. Masuknya pihak bertanggung jawab terhadap akses dalam penggunaan, perubahan dan/atau pengungkapan Akun dari pihak ketiga akibat kelalaian dan/atau kesalahan Member/Verified Member berakibat ditanggungnya resiko oleh Member/Verified Member, maka WALLBROZ tidak akan Memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak lain manapun atas segala resiko, tanggung jawab dan tuntutan apapun yang mungkin timbul sehubungan dengan adanya kelalaian/kesengajaan/kesalahan Member/Verified Member dalam memberikan informasi.
<br>
<br>
KEADAAN KAHAR/FORCE MAJEURE
<br>
<br>
1. Yang dimaksud dengan Force Majeure adalah kejadian-kejadian yang terjadi di luar kemampuan dan kekuasaan WALLBROZ sehingga mempengaruhi pelaksanaan transaksi antara lain namun tidak terbatas pada :
<br>
<br>
a. Gempa bumi, angin topan, banjir, tanah longsor, gunung meletus dan bencana alam lainnya;
<br>
<br>
b. Perang, demonstrasi, huru-hara, terorisme, sabotase, embargo, dan pemogokan massal; c. Kebijakan ekonomi dari Pemerintah yang mempengaruhi secara langsung;
<br>
<br>
2. Sepanjang WALLBROZ telah melaksanakan segala kewajibannya sesuai dengan peraturan perundang-undangan yang berlaku sehubungan dengan terjadinya Force Majeure tersebut, maka WALLBROZ tidak akan memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak lain manapun atas segala resiko, tanggungjawab dan tuntutan apapun yang mungkin timbul sehubungan dengan keterlambatan maupun tidak dapat dilaksanakannya kewajiban akibat Force Majeure.
<br>
<br>
PEMBERITAHUAN
1. Member/Verified Member menyatakan setuju untuk berkomunikasi dengan WALLBROZ dalam format elektronik, dan menyetujui bahwa semua syarat, kondisi, perjanjian, pemberitahuan, pengungkapan atau segala bentuk komunikasi lainnya yang disediakan oleh WALLBROZ kepada Member/Verified Member secara elektronik dianggap sebagai tertulis.
<br>
<br>
2. Member/Verified Member menyatakan setuju untuk menerima email dari Website. E-Mail yang dikirim dapat berisi informasi seputar Akun, transaksi, sistem, promosi dan lain sebagainya.
<br>
<br>
PENYELESAIAN PERSELISIHAN
Setiap perselisihan, sengketa atau perbedaan pendapat (selanjutnya disebut sebagai “Perselisihan”) yang timbul sehubungan dengan pelaksanaan kerjasama akan diselesaikan dengan cara-cara sebagai berikut :
1. Bahwa setiap Perselisihan, sepanjang memungkinkan, akan diselesaikan dengan cara
musyawarah untuk mufakat.
<br>
<br>
2. Setiap Perselisihan yang tidak dapat diselesaikan secara musyawarah, akan diselesaikan
melalui Pengadilan Negeri Jakarta.
<br>
<br>
DOMISILI HUKUM
<br>
<br>
SKU dibuat, ditafsirkan dan diberlakukan berdasarkan hukum negara Republik Indonesia. wallbroz.com dan Member/Verified Member telah sepakat untuk memilih tempat kediaman hukum yang umum dan tidak berubah pada Kantor Kepaniteraan Pengadilan Negeri Jakarta. KETENTUAN LAIN-LAIN :
<br>
<br>
1. Untuk hal-hal yang belum diatur dalam SKU, maka akan berlaku seluruh ketentuan dalam Kitab Undang-Undang Hukum Perdata serta ketentuan-ketentuan peraturan perundang- undangan lainnya yang terkait.
<br>
<br>
2. Jika WALLBROZ melakukan perubahan terhadap isi SKU ini maka WALLBROZ akan memberitahukan perubahannya kepada Member/Verified Member sesuai dengan peraturan perundang-undangan yang berlaku melalui media pemberitahuan yang dianggap baik oleh WALLBROZ dan selanjutnya Member/Verified Member akan tunduk pada perubahan SKU tersebut. Perubahan setiap lampiran dari SKU akan disepakati dan untuk selanjutnya merupakan satu kesatuan dengan dan merupakan bagian yang tidak terpisahkan dari SKU.
<br>
<br>
3. Apabila Member/Verified Member melakukan tindakan-tindakan di luar ketentuan SKU, maka Member/Verified Member akan bertanggung jawab sepenuhnya dan dengan ini setuju bahwa WALLBROZ tidak akan memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member, atau pihak manapun atas segala tuntutan dan/atau gugatan dan/atau klaim yang diajukan pihak lain sehubungan dengan tindakan-tindakan yang dilakukan Member/Verified Member.
<br>
<br>
4. Member/Verified Member wajib mematuhi seluruh persyaratan yang dicantumkan di dalam SKU. Kelalaian Member/Verified Member didalam mentaati atau melaksanakan isi dari SKU pada satu atau beberapa kali kejadian, tidak akan menghilangkan kewajiban Member/Verified Member untuk tetap memenuhi segala persyaratan yang terdapat di dalam SKU.
<br>
<br>
5.Pertanyaan seputar Ketentuan dan Persyaratan atau perihal lain, dilakukan melalui support@wallbroz.com
<br>
<br>
SYARAT DAN KETENTUAN UMUM MEMBER/VERIFIED MEMBER wallbroz.COM INI TELAH DISESUAIKAN DENGAN KETENTUAN PERATURAN PERUNDANG-UNDANGAN YANG BERLAKU.
<br>
<br>
Tingkatkan Keamanan Akun WALLBROZ
<br>
<br>
Jika Anda menemukan aktivitas yang mencurigakan (tidak dikenali dan tidak wajar) di akun WALLBROZ Anda, terdapat kemungkinan bahwa ada orang lain sedang menggunakannya tanpa seizin Anda.
<br>
<br>
PENYEBAB AKUN ANDA DAPAT DIRETAS
<br>
<br>
Ø Melakukan akses dan login pada website phishing yang menyerupai website resmi www.wallbroz.com. <br>
Ø Password terlalu mudah untuk ditebak (nama, tanggal lahir, angka berurutan, nomor telpon) <br>
Ø Menggunakan aplikasi tidak resmi (non-official) dari WALLBROZ. <br>
Ø Menggunakan e-mail dan password yang sama antar website. <br>
Ø Menyimpan data e-mail dan password dalam aplikasi cloud/drive online. <br>
Ø Terdapat malware yang menginfeksi perangkat Anda. Contoh: Keylogger, Virus. <br>
<br>
<br>
INDIKASI AKUN ANDA TIDAK AMAN
<br>
<br>
APA YANG HARUS DILAKUKAN JIKA AKUN ANDA TELAH DIRETAS
<br>
<br>
Ø Adanya aktivitas transaksi keuangan dan aset digital yang mencurigakan (tidak dikenal dan tidak wajar) pada riwayat transaksi Anda. <br>
Ø Perubahan data pada akun seperti e-mail dan no handphone tanpa sepengetahuan Anda. <br>
Ø Segera ganti password di menu lupa password di aplikasi wallbroz.COM <br>
Ø Anda dapat mengunci akun melalui e-mail konfirmasi login yang dikirimkan dan akun akan <br>
dikunci selama 24 jam.
Ø Segera hubungi tim support WALLBROZ melalui e-mail pada alamat support@wallbroz.com dengan mencantumkan kronologinya. <br>
CARA MENJAGA KEMANAN AKUN ANDA
<br>
<br>
Ø Pastikan Anda login hanya pada www.wallbroz.com (bukan dari halaman website phishing). <br>
Ø Buatlah password yang menggunakan kombinasi unik (angka, huruf besar, huruf kecil, dan simbol). <br>
Ø Pastikan semua e-mail terkait akun WALLBROZ hanya dari domain “@wallbroz.com” bukan yang lain. <br>
Ø Lakukan pemeriksaan secara berkala pada riwayat transaksi akun. <br>
Ø Pastikan menggunakan aplikasi resmi dari WALLBROZ saja; <br>
Ø Android WALLBROZ trading platform Dompet WALLBROZ Indonesia; ü iOS WALLBROZ trading platform Dompet WALLBROZ Indonesia; <br>
Ø Jangan pernah menggunakan sandi Akun WALLBROZ Anda di situs lain. <br>
Ø Bookmark www.wallbroz.com pada browser untuk mengantisipasi akses situs phishing. <br>
Ø Update Operating System dan browser secara berkala <br>