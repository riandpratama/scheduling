PROSES REGISTRASI / PENDAFTARAN MEMBER (INFORMASI & DATA MEMBER)<br>
1. Syarat menjadi Member adalah :<br>
Ø Member dapat melakukan proses registrasi atau pendaftaran Member melalui platform WALLBROZ;<br>
Ø Setuju dan sepakat untuk tunduk pada Syarat dan Ketentuan Umum ini;<br>
Ø Berusia minimal 18 (delapan belas) tahun, atau telah menikah; dan<br>
Ø Memiliki identitas yang sah secara hukum.<br>
<br>
<br>
2. Atas setiap proses registrasi, calon Member wajib menunjukkan semua dan setiap data sesuai dengan identitas diri berupa :<br>
Ø Nama (sesuai dengan Identitas diri yang dilampirkan);<br>
Ø Alamat rumah sesuai identitas;<br>
Ø Alamat tinggal saat ini;<br>
Ø Nomor telepon atau Handphone (nomor harus aktif dan digunakan secara pribadi); Ø Tempat dan tanggal lahir (sesuai <br>dengan identitas diri yang dilampirkan);
Kewarganegaraan;
Ø Jenis kelamin;<br>
Ø Foto kartu identitas yang masih berlaku. Kartu identitas yang dapat digunakan adalah:<br>
Kartu Tanda Penduduk (KTP), Surat Ijin Mengemudi (SIM), Paspor, Suket ( Surat
keterangan dari kelurahan Pengganti sementara KTP Elektric )
Ø Pekerjaan;<br>
Ø E-Mail (alamat surat elektronik) yang aktif;<br>
<br>
<br>
3. Dan/atau segala sesuatu yang diminta dan diperlukan berkenaan dengan syarat registrasi yang ditentukan, dan calon Member dengan ini menyatakan serta menjamin bahwa segala data/keterangan/dokumen/informasi/pernyataan apapun yang diberikan berkenaan dengan proses registrasi sebagai Member WALLBROZ adalah lengkap, asli, benar dan sesuai dengan keadaan yang sebenarnya serta merupakan data/keterangan/dokumen/informasi /pernyataan terkini yang tidak/belum dilakukan perubahan dan masih berlaku/tidak daluarsa serta tidak/belum ada perubahan atau kondisi lainnya yang disetujui berdasarkan kebijakan dalam halaman registrasi pada Website.
<br>
<br>
4. Member dengan ini setuju bahwa proses menjadi Member WALLBROZ hanya akan berlaku efektif setelah seluruh persyaratan WALLBROZ dipenuhi oleh Member dan proses registrasi telah melalui proses verifikasi untuk disetujui WALLBROZ. Segala resiko yang timbul sehubungan dengan penutupan/pemblokiran/pembekuan Akun yang diakibatkan oleh kesalahan dan/atau kelalaian Verified Member, akan menjadi tanggung jawab Verified Member dan WALLBROZ tidak akan Memberikan ganti rugi kepada Verified Member atau Pihak manapun dalam bentuk apapun atas segala tuntutan/klaim dan ganti rugi dari Verified Member atau Pihak manapun sehubungan dengan penutupan Akun Verified Member.
<br>
<br>
5. Seluruh data, keterangan, informasi, pernyataan, dokumen yang diperoleh WALLBROZ berkenaan dengan Member/Verified Member, akan menjadi milik WALLBROZ dan WALLBROZ berhak untuk melakukan verifikasi, mencocokkan, menilai, merahasiakan atau menggunakannya untuk kepentingan WALLBROZ sesuai dengan ketentuan hukum yang berlaku tanpa adanya kewajiban WALLBROZ untuk memberitahu atau meminta persetujuan, memberikan jaminan atau ganti rugi dan dengan alasan apapun kepada Member/Verified Member.
<br>
<br>
6. WALLBROZ akan mengatur, mengelola dan melakukan pengawasan menurut tata cara/prosedur yang ditetapkan WALLBROZ atas segala data, keterangan, informasi, pernyataan, dokumen atau segala sesuatu yang berkenaan dengan Member/Verified
Membermaupun kegiatan usaha atau transaksi Member/Verified Memberyang terkait dengan Akun Member/Verified Member.
<br>
<br>
7. Member/Verified Member dengan ini memberikan persetujuan dan kuasa kepada WALLBROZ untuk menggunakan semua data, keterangan dan informasi yang diperoleh WALLBROZ mengenai Member/Verified Membertermasuk namun tidak terbatas pada penggunaan sarana komunikasi pribadi Member/Verified Memberuntuk segala keperluan lainnya sepanjang dimungkinkan dan diperkenankan oleh Perundang – Undangan yang berlaku, termasuk yang bertujuan untuk pemasaran bagi WALLBROZ ataupun bagi pihak lain yang bekerjasama dengan WALLBROZ. Untuk penggunaan data yang memerlukan persetujuan pihak lain, dengan ini Member/Verified Member menyatakan bahwa telah memberikan persetujuan tertulis kepada Pihak Ketiga manapun untuk penggunaan data, keterangan dan informasi tersebut, dan oleh karena itu WALLBROZ dengan ini tidak akan memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member dan pihak manapun atas segala resiko, tuntutan, gugatan dan/atau ganti rugi yang mungkin timbul dikemudian hari sehubungan dengan penggunaan data, keterangan dan informasi yang telah memperoleh persetujuan tertulis tersebut oleh WALLBROZ.
<br>
<br>
IDENTIFIKASI DAN NAMA AKUN MEMBER/VERIFIED MEMBER <br>
1. Setiap Akun yang dibuka akan diadministrasikan oleh WALLBROZ berdasarkan identifikasi khusus menurut nama Member/Verified Memberyang akan berlaku juga sebagai nama/identitas Akun sesuai dengan yang tertera pada identitas Member/Verified Member. Member/Verified Member dilarang untuk menggunakan Akun Member/Verified Member selain Akun milik Member/Verified Member, atau mengakses Akun Member/Verified Member lain, atau membantu orang lain untuk mendapatkan akses tanpa izin ke dalam Akun tersebut. Seluruh penggunaan Akun Member/Verified Member adalah tanggung jawab sepenuhnya dari pemilik Akun yang terdata di dalam sistem Website.
<br>
<br>
2. Member/Verified Member wajib menggunakan dan mencantumkan email Member/Verified Member dan kata sandi yang telah diverifikasi WALLBROZ pada saat registrasi. Sistem WALLBROZ akan menolak secara otomatis proses layanan atas Akun, bilamana tidak disertai dengan email dan kata sandi yang benar. Member/Verified Member bertanggung jawab untuk menjaga kerahasiaan kata sandi, Akun, PIN, akses wallet Aset Digital, akses login email dan segala jenis aktivitas yang meliputi transaksi di dalam Akun Member/Verified Member. Member/Verified Memberbertanggung jawab penuh atas penggunaan kata sandi dan Akun pada WALLBROZ. Apabila terjadi penggunaan sandi
dan/atau Akun tanpa seizin Member/Verified Member dan terjadi dugaan pelanggaran lain, Member/Verified Member wajib menghubungi WALLBROZ dengan mengirimkan e- mail ke support@WALLBROZ.com disertai informasi pendukung. WALLBROZ tidak bertanggung jawab atas kerugian yang ditimbulkan atas adanya penyalahgunaan Akun dan/atau kata sandi, dengan atau tanpa sepengetahuan Member/Verified Member.
<br>
<br>
3. Member/Verified Membersetuju tidak akan menggunakan jasa yang disediakan oleh Website untuk melakukan tindakan kriminal dalam bentuk apapun, termasuk namun tidak terbatas pada, pencucian uang, perjudian, pembelian barang ilegal, kegiatan teroris atau kegiatan hacking. Setiap Member/Verified Member yang melanggar peraturan tersebut dapat diberhentikan dan harus bertanggung-jawab dengan kerugian yang diderita oleh WALLBROZ atau para pengguna lain di dalam Website. WALLBROZ berhak menutup Akun dan membekukan dana, Aset Digital dan transaksi di dalamnya apabila menemukan aktifitas yang mencurigakan di dalam Akun tersebut hingga waktu yang tidak ditentukan.
<br>
<br>
MERUBAH INFORMASI AKUN MEMBER/VERIFIED MEMBER
Member/Verified Member dapat memperbaharui atau merubah informasi Akun miliknya melalui layanan WALLBROZ yang disediakan dan ditetapkan oleh WALLBROZ, yaitu PERUBAHAN DATA MEMBER/VERIFIED MEMBER
1. Perubahan Nama Akun Member/Verified Member
Perubahan Nama Akun Member/Verified Memberdapat dilakukan dengan mekanisme yang diwajibkanuntukmengirimkannamalamadannamabarupemilikAkun,username, e- mail, nomor telepon, dan nomor kartu identitas yang digunakan (KTP/SIM/Passport/Suket). WALLBROZ akan melakukan proses pencocokan data dan akan melakukan konfirmasi ke nomor telepon yang terdaftar di Akun Member/Verified Member untuk menghindari terjadinya penipuan. Jika proses perubahan telah terlewati maka nama Akun akan berubah sesuai keinginan Member/Verified Member. Berkenaan dengan perubahan nama Akun ini, Contoh alasan yang diperbolehkan adalah: ejaan nama salah, ingin mengganti nama menjadi nama orang tua karena belum cukup umur atau menjadi nama suami/istri karena tidak memiliki rekening bank atas nama sendiri. Dalam perihal Apabila nama pemilik Akun masih terdapat hubungan saudara atau orang tua, calon Member/Verified Member diwajibkan untuk mengirimkan softcopy Kartu Keluarga ke support@WALLBROZ.com dengan subjek ‘Data Kelengkapan Penggantian Nama Akun’. Jika hubungan Member/Verified Member dengan pemilik nama tersebut adalah hubungan suami-istri, calon Member/Member/Verified Member diwajibkan untuk mengirimkan softcopy Akta Nikah.
<br>
<br>
2. Perubahan Data Pendukung
Perubahan dan Pembaharuan Data Pendukung wajib dilakukan oleh Member/Verified Member terhadap data berupa nomor telepon, alamat e-mail dan data pendukung lainnya sesuai dengan kondisi terkini. Hal ini sesuai dengan prinsip KYC (Know Your Customer) Principles yang diterapkan oleh WALLBROZ. Perubahan dan Pembaharuan Data dilakukan dengan melakukan pengiriman e-mail dengan subjek ‘Perubahan Data’ ke support@wallbroz.com dengan menuliskan data apa yang ingin disesuaikan oleh Member/Verified Memberdisertai alasan perubahan. Proses perubahan wajib disertai dengan mencantumkan softcopy identitas diri, username, nama lengkap, alamat, e-mail, data lama yang ingin diubah dan data perubahannya. Untuk selanjutnya, WALLBROZ akan memberikan notifikasi dengan menghubungi nomor telepon utama atau nomor telepon alternatif yang telah terdaftar.
<br>
<br>
3. Perubahan Password
Perubahan Password dimungkinkan sebagai alasan keamanan dan mekanismenya telah tersedia dalam layanan..
RUANG LINGKUP KEGIATAN PERDAGANGAN WALLBROZ
Websitehttps://wallbroz.com memperkenankanVerified Memberuntuk melakukan kegiatan- kegiatan perdagangan yang meliputi :
1. Pembelian Aset Digital dengan mata uang Rupiah.<br>
2. Penjualan Aset Digital dengan mata uang Rupiah.<br>
3. Melakukan deposit uang dalam mata uang Rupiah.<br>
4. Melakukan penarikan dalam mata uang Rupiah.<br>
5. Melakukan produksi dan pelayanan pengiriman Voucher WALLBROZ yang dapat dicairkan menjadi saldo rupiah didalam Akun WALLBROZ lainnya.
<br>
<br>
TATA CARA TRANSAKSI WALLBROZ<br>
v Metode Market Maker Merupakan metode dalam layanan dimana Verified Member dapat menentukan harga jual/beli dalam melakukan transaksi perdagangan Aset Digital baik menggunakan Rupiah maupun Aset Digital. <br>
v Metode Market Taker Merupakan metode dalam layanan dimana Verified Member dapat melakukan pembelian/penjualan Aset Digital dengan Rupiah, secara instan atau langsung <br>
lanjut untuk menentukan apakah nama yang muncul dalam segala transaksi yang dilakukan atau akan dilakukan oleh Member/Verified Member melalui Akunnya adalah nama teroris. <br>
<br>
<br>
5. WALLBROZ tidak akan bertanggung jawab untuk setiap kerugian (baik secara langsung dan termasuk namun tidak terbatas pada kehilangan keuntungan atau bunga) atau kerugian yang diderita oleh pihak manapun yang timbul dari segala penundaan atau kelalaian dari WALLBROZ untuk memproses segala perintah pembayaran tersebut atau informasi atau komunikasi lainnya atau untuk melaksanakan segala kewajiban lainnya, yang disebabkan secara keseluruhan atau sebagian oleh segala tindakan yang diambil berdasarkan angka 4 ketentuan Penolakan dan Penundaan Transaksi ini.
<br>
<br>
6. WALLBROZ berwenang untuk melakukan pemantauan atas Akun Member/Verified Member dalam rangka pencegahan kejahatan keuangan.
<br>
<br>
7. Member/Verified Member mengerti, memahami dan setuju bahwa terhadap setiap transaksi yang telah dilakukan melalui WALLBROZ bersifat final dan tidak dapat dilakukan pembatalan oleh Member/Verified Member.
<br>
<br>
<br>
TRANSAKSI MENCURIGAKAN
<br>
1. Dalam hal terjadinya transaksi mencurigakan yang dilakukan melalui layanan WALLBROZ, maka WALLBROZ berhak untuk menghentikan/menonaktifkan Akun WALLBROZ pada Member/Verified Member dan memblokir dana transaksi serta melakukan penundaan transaksi kepada Member/Verified Member, sampai dengan adanya pemberitahuan dari WALLBROZ.
<br>
<br>
2. Dalam hal terjadi transaksi mencurigakan dan/atau transaksi yang melebihi batasan volume transaksi yang ditetapkan oleh WALLBROZ terhadap Member/Verified Member karena alasan apapun juga, maka WALLBROZ berhak sewaktu-waktu menunda pengkreditan dana ke Akun WALLBROZ Member/Verified Member dan/atau melakukan pemblokiran Akun Member/Verified Member sampai proses investigasi selesai dilakukan dalam jangka waktu yang ditentukan oleh WALLBROZ.
<br>
<br>
3. Apabila terbukti bahwa transaksi tersebut pada angka 1 dan 2 tersebut diatas mengenai transaksi mencurigakan adalah transaksi yang melanggar SKU dan/atau peraturan perundang-undangan yang berlaku, maka Member/Verified Member dengan ini Memberi kuasa kepada WALLBROZ untuk mendebet Aset Digital pada Dompet Digital WALLBROZ untuk mengganti kerugian WALLBROZ yang timbul akibat transaksi tersebut, tanpa mengurangi hak untuk melakukan tuntutan ganti rugi atas seluruh kerugian yang timbul akibat transaksi tersebut dan Member/Verified Memberdengan ini setuju bahwa WALLBROZ tidak wajib melakukan pengembalian atas dana yang ditunda pengkreditannya oleh WALLBROZ atau dana yang diblokir sebagaimana dimaksud pada angka 2 ketentuan mengenai transaksi mencurigakan ini.

<br>
<br>
BATAS PENARIKAN
<br>
1. Member/Verified Member dengan ini menyatakan setuju untuk mematuhi batas penarikan baik terhadap Aset Digital maupun Rupiah yang berlaku terhadap Akun Member/Verified Member, dan WALLBROZ yang dalam hal ini menerapkan prinsip Anti Money Laundering (AML) sesuai dengan regulasi Pemerintah Republik Indonesia diberi kuasa serta hak untuk tidak melakukan proses terhadap transaksi yang telah melebihi batas penarikan harian yaitu sebesar yang ditentukan oleh WALLBROZ.
<br>
<br>
2. Batas penarikan direset ulang atau kembali pada angka 0 (nol) setiap pukul 00.00 Waktu Indonesia Barat (WIB).
<br>
<br>
3. Proses persetujuan atau penolakan kenaikan batas penarikan limit harian mutlak merupakan kewenangan WALLBROZ dengan pertimbangan dari tim audit dan hukum sesuai permintaan, sejarah transaksi, sumber dana, dan tujuan penggunaan transaksi serta tidak dapat diintervensi. Member/Verified Memberyang ditolak kenaikan limitnya baru dapat mengajukan permohonan kembali dengan jangka waktu 1×24 (satu kali dua puluh empat) jam.
<br>
<br>
VOUCHER WALLBROZ
<br>
<br>
1. WALLBROZ menyediakan layanan untuk melakukan kegiatan transaksi Aset Digital dalam bentuk voucher WALLBROZ (selanjutnya disebut sebagai “voucher”), baik dalam bentuk pembuatan untuk mitra baru.
LAYANAN PENDUKUNG WALLBROZ
<br>
<br>
WALLBROZ menyediakan layanan dalam bentuk Mobile Application (selanjutnya disebut sebagai “Aplikasi Seluler”), dengan ketentuan sebagai berikut :
<br>
<br>
1. Dalam perihal penggunaan Aplikasi Seluler oleh Member/Verified Member dan pengguna, WALLBROZ berhak untuk meminta dan mengumpulkan informasi berkenaan dengan perangkat
<br>
<br>
seluler yang digunakan Member/Verified Memberuntuk mengakses Aplikasi Seluler yang disediakan termasuk namun tidak terbatas untuk perangkat keras, sistem operasi, pengenal perangkat unik, informasi jaringan seluler juga termasuk data pribadi seperti alamat surat elektronik, alamat, nomor telepon seluler, alias, kata sandi, kode PIN Sellerdan informasi lainnya yang diperlukan untuk menggunakan layanan aplikasi seluler ini;
<br>
<br>
2. Aplikasi Seluler ini telah diuji oleh para peneliti dan Pengembang WALLBROZ dan didalam penggunaannya, WALLBROZ menghimbau untuk berhati-hati didalam pemakaiannya dan WALLBROZ tidak dapat menjamin performa dan Aplikasi Seluler ini selalu berjalan dengan hasil yang dikehendaki;
<br>
<br>
3. WALLBROZ dan Pengembang tidak bertanggung jawab dan tidak terikat untuk segala bentuk keuntungan maupun kerugian yang dimungkinkan terjadi sebagai akibat dari penggunaan Aplikasi Seluler ini. Keuntungan yang dimaksud mengacu pada bertambahnya jumah saldo dalam bentuk format Aset Digital ataupun Rupiah. Sedangkan untuk kerugian yang dimaksud, hal tersebut termasuk namun tidak terbatas untuk: berkurangnya jumlah saldo dalam format Aset Digital dan/atau Rupiah , kegagalan untuk menjalankan Aplikasi Seluler dan segala perintahnya, masalah jaringan/sinyal, terjadinya error pada sistem atau error yang disebabkan karena faktor- faktor dalam bentuk lain;
<br>
<br>
4. Dalam pemakaian Aplikasi Seluler ini, Member/Verified Member ataupun Pengguna telah menyetujui bahwa dalam kondisi apapun, Member/Verified Memberdilarang untuk: menggandakan, melakukan penyalinan, memproduksi ulang, menerjemahkan, mengganti sistem, memodifikasi, melepas pemasangan, melepas susunan atau mencoba untuk menderivasikan kode sumber dari produk perangkat lunak ini;
5. Segala keputusan didalam penggunaan Aplikasi Seluler ini adalah keputusan secara suka rela atau independen yang dibuat oleh Member/Verified Member ataupun Pengguna tanpa adanya paksaan dari WALLBROZ dan Pengembang. Atas perihal ini, Member/Verified Member ataupun Pengguna melepaskankan WALLBROZ dan Pengembang dari segala bentuk tuntutan, ganti rugi dan segala tanggung jawab dalam bentuk apapun;
<br>
<br>
6. Member/Verified Memberatau Pengguna menyatakan telah mengerti batasan-batasan keamanan dan privasi namun tidak terbatas kepada :
(i) batasan ukuran dan fitur keamanan, privasi, dan autentikasi dalam layanan;<br>
(ii) segala data dan informasi di dalam layanan mungkin dapat mengarah ke penyadapan, pemalsuan, spam, sabotase, pembajakan kata sandi, gangguan, penipuan, penyalahgunaan data elektronik, peretasan, dan kontaminasi sistem, termasuk namun tanpa pembatasan, virus, worms, dan Trojan horses, yang menyebabkan ketidak absahan, kerusakan, maupun akses yang berbahaya, dan/atau pemulihan informasi atau data dalam komputer Member/Verified Memberatau bahaya keamanan dan privasi lainnya. Apabila Member/Verified Membertidak menghendaki untuk dikenai resiko – resiko tersebut, Member/Verified Memberdisarankan untuk sangat berhati-hati di dalam penggunaan Aplikasi Seluler maupun layanan ini.
<br>
<br>
KERJASAMA LAYANAN
<br>
<br>
1. WALLBROZ tidak bekerjasama dalam hal apapun dengan perusahaan-perusahaan Arisan Berantai, cloud mining, MLM, Ponzi Scheme, Money Game dan HYIP yang menawarkan profit dari perdagangan atau penggunaan Aset Digital, dan tidak bekerjasama dengan Biclubnetwork, BTCPanda, MMM Global, MMM Indonesia, Onecoin, Binary, BITCOINTRUST2U, BTCPANDA, BITKINGDOM, BITCLUBNETWORK, EUROBIT, ILGAMOS, FUTURENET dan tidak bertanggung jawab atas penggunaan layanan tersebut diatas oleh Member/Verified Member;
<br>
<br>
2. Perusahaan yang tidak terdata didalam Website dapat diartikan sebagai tidak ataupun belum berkerjasama dengan WALLBROZ. WALLBROZ tidak merekomendasi perusahaan manapun termasuk perusahaan yang bekerjasama dengan WALLBROZ. Segala keputusan untuk menggunakan layanan tersebut menjadi tanggung jawab masing- masing Pengguna.
<br>
<br>
3. WALLBROZ dengan ini menjelaskan bahwa tidak memiliki afiliasi resmi dengan partner yang telah disebutkan pada ayat (3) diatas. Segala transaksi yang terjadi antara Member/Verified Member dengan perusahaan-perusahaan di atas bukan merupakan tanggung jawab WALLBROZ.
<br>
<br>
STANDAR PENANGANAN PENGADUAN
<br>
<br>
Pengaduan Member/Verified Member disini terkait dengan pemberitahuan pelanggaran data dan/atau apabila terjadi permasalahan berkenaan dengan kepemilikan Aset Digital yang dimiliki oleh Member/Verified Member. Apabila terjadi permasalahan tersebut, berikut adalah mekanisme yang harus dilakukan oleh Member/Verified Member yaitu :
<br>
<br>
1. Berkenaan dengan pengaduan Member/Verified Member, Member/Verified Member memiliki hak untuk melakukan pemberitahuan kepada WALLBROZ dengan kondisi sebagai berikut:
<br>
<br>
a. Apabila Member/Verified Member menerima spam, segala bentuk iklan liar dan/atau surat elektronik yang mengatasnamakan nama selain WALLBROZ yang meminta data pribadi dan/atau mengganggu kenyamanan para Member/Verified Member;
<br>
<br>
b. Apabila terdapat praktik tindak kejahatan pencurian dan penipuan Aset Digital yang mengakibatkan hilangnya Aset Digital di Akun Member/Verified Member;
<br>
<br>
c. Apabila terdapat dugaan aktivitas pelanggaran data Member/Verified Member oleh pihak lain yang bukan dijelaskan pada poin a dan b tersebut diatas.
<br>
<br>
2. Member/Verified Member dapat mengajukan pengaduan dengan mengisi di menu pesan yang tersedia di aplikasi
<br>
<br>
3. Berkenaan dengan praktek penyalahgunaan data Member/Verified Member oleh pihak lain yang mengakibatkan adanya spam atau praktek skimming, maka Member/Verified Member dimohon dapat melakukan pengaduan dengan mekanisme sebagai berikut:
<br>
<br>
a. Sebagaimana yang diisyaratkan pada poin 2, Member/Verified Memberdapat melakukan pengaduan dengan mengisi di menu pesan yang tersedia di aplikasi.
<br>
<br>
b. Laporan tersebut akan segera kami lakukan tindak lanjut dengan penanganan pengaduan dalam kurun waktu 3 x 24 jam;
<br>
<br>
c. Apabila dalam kurun waktu tersebut, Member/Verified Member tidak mendapatkan pelayanan dari pihak kami, maka Member/Verified Member dapat mengirimkan kembali pengaduan tersebut ke email support@WALLBROZ.com dan/atau melakukan kontak langsung ke kantor representative WALLBROZ;
<br>
<br>
4. Berkenaan dengan hilangnya Aset Digital di Akun Member/Verified Member sebagai akibat adanya praktik tindak pidana kejahatan elektronik oleh pihak lain, maka Member/Verified Member dapat melakukan pengaduan dengan mekanisme sebagai berikut:
<br>
<br>
a. Sebagaimana yang diisyaratkan pada poin 2, Member/Verified Memberdapat melakukan pengaduan melalui pengisian pesan pengaduan yang telah disediakan.;
<br>
<br>
b. Laporan tersebut akan dilaksanakan penangan awal dimana akan dilakukan pembekuan
Akun oleh pihak terlapor (pihak terduga) yang menampung atau mengambil Aset Digital
Member/Verified Membertersebut;
<br>
<br>
c. Admin akan meneruskan laporan pengaduan anda kepada tim legal untuk memproses
penanganan selanjutnya. Penanganan selanjutnya berupa klarifikasi dengan pihak
terlapor (pihak terduga);
<br>
<br>
d. Apabila tahap klarifikasi ini berhasil dimana pihak terlapor Memberikan respon maka
akan dilakukan mediasi dengan pihak pelapor (dalam hal ini Member/Verified
Member yang melapor);
<br>
<br>
e. Apabila mediasi tidak berhasil maka akan dilanjutkan melalui laporan ke kepolisian
setempat dan kami akan mempersiapkan data yang diminta.
<br>
<br>
PERNYATAAN DAN JAMINAN
<br>
<br>
1. Semua layanan dalam Websitetidak Memberikan jaminan ataupun garansi apapun dan WALLBROZ tidak menjamin bahwa Website akan selalu dapat diakses setiap waktu.
<br>
<br>
2. Member/Verified Member menyatakan dan menjamin akan menggunakan layananWALLBROZ dengan baik dan penuh tanggung jawab serta tidak melakukan tindakan yang berlawanan dengan hukum, undang-undang serta peraturan yang berlaku di wilayah Republik Indonesia.
<br>
<br>
3. Member/Verified Member menyatakan dan menjamin tidak akan menggunakan layanan WALLBROZ dalam hal penjualan barang dan/atau jasa yang berhubungan dengan :
<br>
<br>
a. Narkotika, bahan-bahan dan/atau zat-zat kimia untuk penelitian,<br>
b. Uang dan apapun yang sejenis dengan uang, termasuk derivativenya,<br>
c. Barang dan/atau jasa yang melanggar Hak Kekayaan Intelektual,<br>
d. Amunisi, senjata api, bahan peledak, serta senjata tajam yang termasuk di dalam
Ketentuan Hukum yang berlaku,<br>
e. Barang dan/atau jasa yang menunjukkan informasi pribadi dari Pihak Ketiga yang
melanggar hukum,<br>
f. Dukungan terhadap skema Ponzi,<br>
g. Barang dan/atau jasa yang berhubungan dengan pembelian lotre (layaway),<br>
h. Jasa yang terkait dengan perbankan yang berada di luar wilayah Republik Indonesia,<br>
i. Dukungan terhadap organisasi terlarang atau dilarang oleh pemerintah.<br>
4. Member/Verified Member menyatakan dan menjamin untuk tidak menggunakan layanan WALLBROZ dalam praktek perjudian dan/atau kegiatan lain yang mengenakan biaya masuk dan menjanjikan hadiah, termasuk namun tidak terbatas pada permainan kasino, perjudian dalam olahraga, usaha yang memfasilitasi perjudian dengan cara undian.<br>
5. Member/Verified Member menyatakan dan menjamin bahwa seluruh data, informasi dan dokumen yang diberikan Member/Verified Member kepada WALLBROZ merupakan data, informasi dan dokumen yang sebenar-benarnya, sah, jujur, transparan, lengkap dan akurat. Member/Verified Member menyatakan bersedia dituntut secara pidana maupun perdata apabila WALLBROZ mengetahui atau memperoleh informasi dari pihak manapun bahwa data, informasi dan dokumen yang diberikan Member/Verified Member ternyata tidak benar/tidak sepenuhnya benar/palsu. Member bersedia untuk melakukan pembaharuan data/informasi (profile update) apabila sewaktu-waktu diminta oleh WALLBROZ dan selanjutnya seluruh dokumen yang sudah diberikan menjadi sepenuhnya milik WALLBROZ.<br>
6. WALLBROZ telah Memberikan keterangan dan penjelasan yang cukup mengenai layanan WALLBROZ yang akan dipergunakan Member/Verified Member sesuai dengan ketentuan pada SKU dan Member/Verified Member telah mengerti dan memahami serta bersedia menanggung segala konsekuensi yang mungkin timbul sehubungan dengan penggunaan layanan WALLBROZ termasuk manfaat, resiko dan biaya-biaya yang melekat layanan dan layanan.<br>
7. Member/Verified Member dengan ini menyetujui dan memberi kuasa kepada WALLBROZ untuk menggunakan semua data, keterangan dan informasi yang diperoleh WALLBROZ mengenai Member/Verified Membertermasuk namun tidak terbatas pada penggunaan sarana komunikasi pribadi Member/Verified Member untuk segala keperluan lainnya sepanjang dimungkinkan dan diperkenankan oleh perundang-undangan yang berlaku, termasuk yang bertujuan untuk pemasaran produk-produk WALLBROZ ataupun pihak lain, yang bekerjasama dengan WALLBROZ. Untuk penggunaan data yang memerlukan persetujuan pihak lain, dengan ini Member/Verified Member menyatakan bahwa telah memperoleh persetujuan tertulis dari pihak ketiga manapun untuk penggunaan data, keterangan dan informasi tersebut, dan karena itu Member/Verified Member menjamin dan menyetujui bahwa WALLBROZ tidak akan Memberikan ganti rugi dan/atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala resiko, kerugian, tuntutan dan/atau tanggungjawab yang mungkin timbul dikemudian hari sehubungan dengan penggunaan data, keterangan dan informasi yang telah memperoleh persetujuan tertulis tersebut oleh WALLBROZ.<br>
8. Member/Verified Member menyatakan dan menjamin bahwa transaksi yang dilakukan dengan menggunakan layanan WALLBROZ adalah transaksi yang tidak melanggar ketentuan peraturan perundang-undangan yang berlaku dan ketentuan mengenai penerimaan pelaksanaan transaksi sebagaimana diatur dalam SKU. Dalam hal WALLBROZ menemukan indikasi pelaksanaan transaksi yang tidak sesuai dengan ketentuan peraturan perundang- undangan yang berlaku dan atau ketentuan mengenai penerimaan pelaksanaan transaksi yang diatur dalam SKU, maka WALLBROZ mempunyai hak penuh untuk menutup Akun Member/Verified Member dengan menonaktifkan layanan WALLBROZ pada Member/Verified Member, dan Member/Verified Member dengan ini setuju bahwa WALLBROZ dengan ini tidak akan Memberikan ganti rugi dan atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member, atau pihak manapun atas segala klaim dan atau tuntutan yang timbul sehubungan dengan penonaktifan layanan WALLBROZ pada Member/Verified Member.<br>
9. Member/Verified Member menyatakan dan menjamin bahwa resiko terhadap penggunaan layanan, produk dan promosi Pihak Ketiga dengan Member/Verified Member (apabila ada), ditanggung oleh Member/Verified Member, dan Member/Verified Membermenyatakan bahwa WALLBROZ tidak bertanggung jawab atas layanan dan kinerja layanan Pihak Ketiga.<br>
10. Member/Verified Member dengan ini bertanggung jawab sepenuhnya dan setuju bahwa WALLBROZ tidak akan Memberikan ganti rugi dan atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala kerugian dan atau klaim dan/atau tuntutan yang timbul atau mungkin dialami oleh WALLBROZ sebagai akibat dari kelalaian atau kegagalan Member/Verified Member dalam menjalankan transaksi.<br>
11. Member/Verified Member dengan ini Memberikan jaminan kepada WALLBROZ bahwa Member/Verified Member beserta dengan seluruh karyawannya dan/atau pihak lain yang bekerja sama dengan Member/Verified Membertidak akan memperbanyak dan/atau membuat, memberikan, menyewakan, menjual, memindahkan, mengalihkan, dan/atau mengalih-fungsikan layanan WALLBROZ baik sebagian atau seluruhnya kepada pihak lain dengan alasan apapun, termasuk namun tidak terbatas pada penyalahgunaan layanan WALLBROZ.com untuk melakukan transaksi selain dari yang telah ditentukan dalam SKU dengan maksud apapun, termasuk namun tidak terbatas untuk kejahatan/penipuan/kecurangan. Apabila Member/Verified Member melanggar ketentuan tersebut, maka Member/Verified Member wajib bertanggung jawab atas segala kerugian, tuntutan dan atau gugatan yang timbul akibat dari pelanggaran tersebut dan dengan ini setuju bahwa WALLBROZ tidak akan Memberikan ganti rugi dan atau pertanggungjawaban dalam bentuk apapun kepada Member/Verified Member atau pihak manapun atas segala klaim, tuntutan dan/atau gugatan yang timbul akibat pelanggaran tersebut<br>
12. Sebagai wujud komitmen dan bentuk kepatuhan kami terhadap GDPR (General Data Protection Regulation), maka bersama dengan Syarat dan Ketentuan Umum (SKU) ini WALLBROZ menjamin bahwa :<br>
a. WALLBROZ selalu berkomiten dan menjamin data personal Member/Verified Member kami dan akan menindak tegas apabila ada pihak lain yang telah menggunakan data informasi Member/Verified Member kami,<br>
b. WALLBROZ menjamin transfer lintas batas Aset Digital,<br>
c. WALLBROZ selalu menerapkan protokoler dan mekanisme berupa permintaan ijin dan persetujuan atau notifikasi kepada Member/Verified Memberuntuk segala bentu  pemrosesan data di marketplace didalam melakukan deposit, penarikan dan/atau pembaharuan data.<br>