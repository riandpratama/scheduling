@extends('adminlte::page')

@section('title', 'Konfirmasi Top Up')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Konfirmasi Topup</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
		          		@if (Session::has('success'))
				            <div class="alert alert-success">
				                {{ Session::get('success') }}
				            </div>
				        @endif
				        @if (Session::has('warning'))
				            <div class="alert alert-warning">
				                {{ Session::get('warning') }}
				            </div>
				        @endif
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Faktur</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Chip</center>
				                        </th>
				                        <th>
				                            <center>Biaya</center>
				                        </th>
				                        <th>
				                            <center>Status</center>
				                        </th>
				                        <th>
				                            <center>Konfirmasi</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ $item->faktur }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td><center>{{ $item->coin }}</center></td>
				                		<td><center>Rp.{{ number_format($item->biaya, 0, ',', '.') }}</center></td>
				                		<td>
				                			<center>
				                			@if ($item->isActive == 1)
				                				<p style="color:red;">Belum dibayarkan</p>
				                			@elseif ($item->isActive == 2)
				                				<p style="color:blue;">Proses</p>
				                			@elseif ($item->isActive == 3)
				                				<p style="color:green;">Terkonfirmasi</p>
				                			@else
												<p>-</p>
				                			@endif
				                			</center>
				                		</td>
				                		<td>
				                			<center>
				                				@if ($item->isActive == 1)
				                				<p data-placement="top" data-toggle="tooltip" title="Edit">
				                					<button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-ok"></span>
				                					</button>
				                				</p>
				                				@else
				                					<span class="glyphicon glyphicon-ok"></span>
				                				@endif
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
		          <div class="modal-body">

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('faktur') ? ' has-error' : '' }}">
		                    <label for="faktur">Faktur</label>
		                    <input id="faktur" type="text" readonly="" class="form-control" name="faktur">
		                    @if ($errors->has('faktur'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('faktur') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group{{ $errors->has('coin') ? ' has-error' : '' }}">
		                    <label for="coin">Jumlah Chip</label>
		                    <input id="coin" type="text" readonly="" class="form-control" disabled="">
		                    @if ($errors->has('coin'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('coin') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group{{ $errors->has('biaya') ? ' has-error' : '' }}">
		                    <label for="biaya">Jumlah Biaya</label>
		                    <input id="biaya" type="text" readonly="" class="form-control" disabled="">
		                    @if ($errors->has('biaya'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('biaya') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group{{ $errors->has('upload_bukti') ? ' has-error' : '' }}">
		                    <label for="upload_bukti">Bukti Transfer/Scan Bukti</label>

		                    <input type="file" name="upload_bukti" id="foto" accept="image/*"> 
		                    <br>
	        				<img id="foto_bukti" src="#" alt="your image" style="margin-bottom: 20px" />
		                    @if ($errors->has('upload_bukti'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('upload_bukti') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                
		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin submit?')">
		        				<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Submit
		        			</button>
		        			
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("konfirmasi-topup/store") }}/'+uuid+'/'+item.id+'/update');
        $('#formEdit .form-group #faktur').val(item.faktur);
        $('#formEdit .form-group #coin').val(item.coin);
        $('#formEdit .form-group #biaya').val(format(item.biaya));
    }

    var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(",");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return("Rp " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};
</script>

<script type="text/javascript">
	function readURL() {
		$('#foto_bukti').attr('src', '').hide();
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			$(reader).on('load', function(e) {
				$('#foto_bukti').attr('src', e.target.result)
			});
			reader.readAsDataURL(this.files[0]);
		}
	}
	$('#foto_bukti')
		.on('load', function(e) {
			$(this).css('height', '200px').show();
		})
		.hide();
	$("#foto").on('change', readURL);
</script>
@endsection