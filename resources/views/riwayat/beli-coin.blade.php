@extends('adminlte::page')

@section('title', 'Riwayat Beli Chip')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Beli Chip</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
		          	
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Faktur</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Chip</center>
				                        </th>
				                        <th>
				                            <center>Biaya</center>
				                        </th>
				                        <th>
				                            <center>Status</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ $item->faktur }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td><center>{{ $item->coin }}</center></td>
				                		<td><center>Rp.{{ number_format($item->biaya, 0, ',', '.') }}</center></td>
				                		<td>
				                			<center>
				                			@if ($item->isActive == 0)
				                				<p style="color:blue;">Proses</p>
				                			@elseif ($item->isActive == 1)
				                				<p style="color:green;">Terkonfirmasi</p>
				                			@elseif ($item->isActive == 2)
				                				<p style="color:red;">DiTolak</p>
				                			@else
												<p>-</p>
				                			@endif
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Konfirmasi Topup</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
			        @if (Session::has('warning'))
			            <div class="alert alert-warning">
			                {{ Session::get('warning') }}
			            </div>
			        @endif

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('faktur') ? ' has-error' : '' }}">
		                    <label for="faktur">Faktur</label>
		                    <input id="faktur" type="text" readonly="" class="form-control" name="faktur">
		                    @if ($errors->has('faktur'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('faktur') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group{{ $errors->has('upload_bukti') ? ' has-error' : '' }}">
		                    <label for="upload_bukti">Bukti Transfer/Scan Bukti</label>

		                    <input type="file" name="upload_bukti" id="foto" accept="image/*"> 
		                    <br>
	        				<img id="foto_bukti" src="#" alt="your image" style="margin-bottom: 20px" />
		                    @if ($errors->has('upload_bukti'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('upload_bukti') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                
		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-check" aria-hidden="true" onclick="return confirm('Anda yakin ingin konfirmasi pelanggan?'")></span> Submit</button>

		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')

<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('#formEdit .form-group #faktur').val(item.faktur);
    }
</script>

@endsection