@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <!-- /.login-logo -->
        @if (Session::has('Success'))
            <div class="alert alert-success">
                {{ Session::get('Success') }}
            </div>
        @endif
        @if (Session::has('Info'))
            <div class="alert alert-info">
                {{ Session::get('Info') }}
            </div>
        @endif
        @if (Session::has('Danger'))
            <div class="alert alert-danger">
                {{ Session::get('Danger') }}
            </div>
        @endif

        <div class="login-box-body">
            <div style="font-size: 26px; text-align: center;">
                <img src="{{ asset('assets/images/logo-wbz.png') }}" style="width: 25%;"> 
                <br>
                <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><b>WALLBROZ DISTRICT</b></a>
            </div>
            <br>
            <p class="login-box-msg" style="font-size: 10px;">Harap periksa bahwa Anda mengunjungi URL yang benar</p>
            <p class="login-box-msg" style="font-size: 10px;"><span style="color:green;"><i class="fa fa-lock"></i> https:</span>//wallbroz.com</p>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post" id="login-user">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <div id="captcha"></div>
                </div>
                <div>
                    <!-- /.col -->
                    <div class="">
                        <button type="button" class="btn btn-success btn-block btn-flat " 
                                onclick="verifyCaptcha(grecaptcha.getResponse(widget))">
                            LOG IN
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br>
            <div style="border: 0.9px solid gray;"></div>
            <br>
            {{-- <p>
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                    {{ trans('adminlte::adminlte.i_forgot_my_password') }}
                </a>
            </p> --}}
            @if (config('adminlte.register_url', 'register'))
                <p>
                    <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                       <i class="fa fa-envelope"></i> Lupa Password
                    </a>
                    <a href="{{ route('kebijakan') }}" class="pull-right">
                        <i class="fa fa-user-plus"></i> Register Akun Baru
                    </a>
                </p>
            @endif
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script type="text/javascript">
        var widget;
        var onloadCallback = function () {
            widget =
                grecaptcha.render('captcha', {
                    'sitekey': '6LcencYZAAAAAD0cQA4xIaUM6p4RFkBJI5r61iEq',
                    'theme': 'light'
                });
        };
        function verifyCaptcha(result) {
            if (result !== "") {
                $('#login-user').submit();
            } else {
                alert('Harap isi reCAPTCHA terlebih dahulu');
            }
        }
    </script>
@stop

@section('adminlte_js')
    @yield('js')
@stop
