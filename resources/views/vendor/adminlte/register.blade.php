@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
    <style>
        .register-box-body {
          background: #249c49 !important;
          padding: 20px;
          border-top: 0;
          color: white;
        }
    </style>    
@stop

@section('body_class', 'register-page')

@section('body')
    <div class="register-box">
        @if (Session::has('Danger'))
            <div class="alert alert-danger">
                {{ Session::get('Danger') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <p>Inputan Belum Benar, Harap diisi!</p>
            </div>
        @endif
        <div class="register-box-body">
            <div style="font-size: 26px; text-align: center;">
                <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><b>WALLBROZ DISTRICT</b></a>
            </div>
            <div class="text-center">
                <p><b>Pendaftaran Akun Personal</b></p>
                <p>Silahkan isi data di bawah ini dengan informasi yang jujur dan akurat.</p>
            </div>
            <br>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post" id="login-user">
                {{ csrf_field() }}
                
                <div class="form-group has-feedback {{ $errors->has('nama_sponsor') ? 'has-error' : '' }}">
                    <label for="">Kode Sponsor : <i style="color: red">*</i></label>
                    <input type="text" name="nama_sponsor" class="form-control" value="{{ Session::get('nama_sponsor') }}"
                           placeholder="Masukkan Kode Sponsor">
                    <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
                    @if ($errors->has('nama_sponsor'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_sponsor') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                    <label for="">Username : <i style="color: red">*</i></label>
                    <input type="text" name="username" class="form-control" value="{{ Session::get('username') }}"
                           placeholder="Masukkan Username">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="">Email :<i style="color: red">*</i></label>
                    <input type="email" name="email" class="form-control" value="{{ Session::get('email') }}"
                           placeholder="Masukkan Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="">Password : <i style="color: red">*</i></label>
                    <input type="password" name="password" class="form-control"
                           placeholder="Masukkan Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label for="">Konfirmasi Password : <i style="color: red">*</i></label>
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="Masukkan Ulang Password">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="checkbox" required /> 
                    <b>Saya setuju dengan 
                        <span data-title="Info" data-toggle="modal" data-target="#info">
                            <u>Syarat dan Ketentuan</u>
                        </span>
                    </b>
                </div>
                <div class="form-group has-feedback">
                    <div id="captcha"></div>
                </div>
                <button type="button" class="btn btn-success btn-block btn-flat" onclick="verifyCaptcha(grecaptcha.getResponse(widget))">
                    Registrasi
                </button>
            </form>
            <br>
            <p>
                <a href="{{ url(config('adminlte.login_url', 'login')) }}" class="text-center" style="color:white;">
                    Saya sudah mempunyai akun, <u> Login disini </u>
                </a>
            </p>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
    
    <div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="info" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>SYARAT-SYARAT DAN KETENTUAN UMUM</h4>
                </div>
                <div class="modal-body">
                    @include('syarat-ketentuan.index')
                    @include('syarat-ketentuan.proses-registrasi')
                    @include('syarat-ketentuan.tanggungjawab')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Setujui</button>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script type="text/javascript">
        var widget;
        var onloadCallback = function () {
            widget =
                grecaptcha.render('captcha', {
                    'sitekey': '6LcencYZAAAAAD0cQA4xIaUM6p4RFkBJI5r61iEq',
                    'theme': 'light'
                });
        };
        function verifyCaptcha(result) {
            if (result !== "") {
                $('#login-user').submit();
            } else {
                alert('Harap isi reCAPTCHA terlebih dahulu');
            }
        }
    </script>
@stop

@section('adminlte_js')
    @yield('js')
@stop
