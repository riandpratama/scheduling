@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body">
            <div style="font-size: 26px; text-align: center;">
                <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><b>WALLBROZ DISTRICT</b></a>
            </div>
            <p class="login-box-msg" style="font-size: 10px;">Harap periksa bahwa Anda mengunjungi URL yang benar</p>
            <p class="login-box-msg" style="font-size: 10px;"><span style="color:green;"><i class="fa fa-lock"></i> https:</span>//wallbroz.com</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ isset($email) ? $email : old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-success btn-block btn-flat">
                    {{ trans('adminlte::adminlte.send_password_reset_link') }}
                </button>
                <a href="/" class="btn btn-warning btn-block btn-flat"> <i class="fa fa-arrow-left"></i> Kembali ke Halaman Awal</a>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop
