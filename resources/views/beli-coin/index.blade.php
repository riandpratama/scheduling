@extends('adminlte::page')

@section('title', 'Beli Chip')

@section('content')

	<div class="col-md-12 row">
        <div class="box box-success">
            <div class="box-header with-border">
                <h5 class="box-title">Informasi</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div>
                    <p>Harga 1 Chip =  Rp<b> 25.000 </b></p>
                </div>
            </div>
        </div>
    </div>
	<div class="box box-success">
        <form role="form" action="{{ route('beli-coin.store', Auth::user()->id) }}" method="POST" enctype= multipart/form-data>
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
		        <div class="form-group">
	              	<label for="username">Akun user</label>
	              	<input type="text" class="form-control" id="username" value="{{ Auth::user()->username }}" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="username">Username Member Pemberi Chip <i style="color:red">*</i></label>
	              	<input type="text" class="form-control" id="username" value="" name="penjual" >
	            </div>
	            
	            <div class="form-group">
	              	<label for="coin">Biaya Rupiah</label>
	              	<div class="input-group">
	                	<span class="input-group-addon">IDR</span>
	                	<input type="text" class="form-control coin" name="biaya" id="coin" placeholder="0" >
              		</div>
	            </div>
	            <div class="form-group">
	              	<label for="coin">Jumlah Chip</label>
	              	<input type="numeric" class="form-control idr" name="coin" readonly="">
	            </div>
	            <div class="form-group{{ $errors->has('upload_bukti') ? ' has-error' : '' }}">
                    <label for="upload_bukti">Bukti Transfer/Scan Bukti</label>

                    <input type="file" name="upload_bukti" id="foto" accept="image/*"> 
                    <br>
    				<img id="foto_bukti" src="#" alt="your image" style="margin-bottom: 20px" />
                    @if ($errors->has('upload_bukti'))
                        <span class="help-block">
                            <strong>{{ $errors->first('upload_bukti') }}</strong>
                        </span>
                    @endif
                </div>
	        </div>

	        <div class="box-footer">
	        	@if (!is_null($beli))
	            <p style="color:red;">Pembelian chip anda sedang diproses, mohon tunggu verifikasi.</p>
	            @elseif (Auth::user()->ultracoin->ripening == 1)
	            <p style="color:red;">Akun Saldo Chip anda masih dalam proses ripening, beli chip dapat dilakukan besok hari.</p>
	            @else
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	            @endif
	       	</div>
        </form>
     </div>
@endsection

@section('js')
<script type="text/javascript">

	var format = function(num){
	    var str = num.toString(), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(",");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};
	
	$(".coin").keyup(function(){
	    var coin = $("#coin").val();
	    var noCommas = $('#coin').val().replace(/,/g, '');
    	console.log(noCommas)
	    var jumlah = (parseInt(coin) / 25000);
	    if(isNaN(jumlah)){
	      jumlah = 0;
	    }
	    $(".idr").val(format(jumlah));
	});
</script>
<script type="text/javascript">
	function readURL() {
		$('#foto_bukti').attr('src', '').hide();
		if (this.files && this.files[0]) {
			var reader = new FileReader();
			$(reader).on('load', function(e) {
				$('#foto_bukti').attr('src', e.target.result)
			});
			reader.readAsDataURL(this.files[0]);
		}
	}
	$('#foto_bukti')
		.on('load', function(e) {
			$(this).css('height', '200px').show();
		})
		.hide();
	$("#foto").on('change', readURL);
</script>
@endsection