@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
	
    <div class="col-md-3">
        <div class="box box-success">
            <div class="box-header with-border">
                <h5 class="box-title">Support</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            
            <div class="box-body">
                <div>
                    {!! $support->konten !!}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="box box-success">
            <div class="box-header with-border">
                <h5 class="box-title">Informasi</h5>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            
            <div class="box-body">
                <div>
                    {!! $informasi->konten !!}
                </div>
            </div>
        </div>
    </div>

	<div class="col-md-3">
	    <div class="box box-success box-solid">
	        <div class="box-header with-border">
	          	<h3 class="box-title">User Akun</h3>
	          	<div class="box-tools pull-right">
	            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	            	</button>
	          	</div>
	        </div>
	        <div class="box-body" style="">
	          	<b style="font-size: 16px;">{{ Auth::user()->username }}</b>
	          	<p style="position: absolute; right: 0; bottom: 0; padding-right: 10px;">Join: {{ date('d/m/Y', strtotime(Auth::user()->created_at)) }}</p>
	        </div>
	    </div>
    </div>

    <div class="col-lg-12 col-xs-12">
    	<div id="chart-container"></div>
    </div>
    <div class="col-lg-12 col-xs-12">
    	<br>
    </div>

    <div class="col-lg-3 col-xs-6">
      	<div class="small-box bg-aqua">
        	<div class="inner">
        		@if (Auth::user()->isActive == 1)
          		<h3>Aktif</h3>
          		@else
          		<h3 style="font-size: 19px; padding: 10px;">Tidak Aktif</h3>
          		@endif
        	</div>
        	<a href="#" class="small-box-footer"><b>Status Akun</b></a>
      	</div>
    </div>

    <div class="col-lg-3 col-xs-6">
      	<div class="small-box bg-green">
        	<div class="inner">
          		<h3>{{ $saldo->coin }}</h3>
        	</div>
        	<a href="#" class="small-box-footer"><b>Saldo Chip</b></a>
      	</div>
    </div>

    <div class="col-lg-3 col-xs-6">
      	<div class="small-box bg-yellow">
        	<div class="inner">
        		@if (is_null($ripening))
        		<h3>0</h3>
        		@else
          		<h3>{{ $ripening }}</h3>
          		@endif
        	</div>
        	<a href="#" class="small-box-footer">Top up Chip</i></a>
      	</div>
    </div>
    <div class="col-lg-3 col-xs-6">
      	<div class="small-box bg-red">
        	<div class="inner">
          		@if ($mining == 'Tidak Aktif')
                <h3 style="font-size: 19px; padding: 10px;">Tidak Aktif</h3>
                @else
                <h3>Aktif</h3>
                @endif
        	</div>
        	<a href="#" class="small-box-footer">Status Mining</a>
      	</div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="small-box bg-purple">
            <div class="inner">
                @if (is_null($temporary))
                <h3>0</h3>
                @else
                <h3>{{ $temporary }}</h3>
                @endif
            </div>
            <a href="#" class="small-box-footer">Chip Ripening</a>
        </div>
    </div>
@stop

@section('js')
<script type="text/javascript">
    	var BASE_URL_API = window.location.origin+'/api/service';
    	var data = "{{ Auth::user()->id }}";
		var jsonify = res => res.json();
		var dataFetch = fetch(
		  `${BASE_URL_API}/coin/${data}`
		).then(jsonify);
		var schemaFetch = fetch(
		  `${BASE_URL_API}/title`
		).then(jsonify);
		
		Promise.all([dataFetch, schemaFetch]).then(res => {
		    const data = res[0];
		    const schema = res[1];
		    const fusionDataStore = new FusionCharts.DataStore();
		    const fusionTable = fusionDataStore.createDataTable(data, schema);

		    $("document").ready(function() {
		        $("#chart-container").insertFusionCharts({
		            type: "timeseries",
		            width: "100%",
		            height: '300',
		            dataFormat: "json",
		            dataSource: {
		                data: fusionTable,
		                chart: {},
		                subcaption: {},
		            }
		        });
		    }); 
		});
	</script>

@endsection