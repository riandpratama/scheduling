@extends('adminlte::page')

@section('title', 'Ambil Chip Bonus')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Ambil Chip Bonus</h3>
		          	<hr>
		          	@if (!is_null($totalChip->total))
		          	<button 
		          		class="btn btn-success" 
		          		data-title="Edit" data-toggle="modal" 
		          		data-target="#edit" 
		          		onclick="showEdit(this);" >
		          		<span class="glyphicon glyphicon-ok"></span> Ambil Chip
		          	</button>
		          	@endif
		          	
		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
	          		@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Username Member</center>
			                        </th>
			                        <th>
			                            <center>Chip</center>
			                        </th>
			                        <th>
			                            <center>Status</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($data as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td><center>{{ $item->user->username }}</center></td>
			                		<td><center>{{ $item->coin }}</center></td>
			                		<td>
			                			<center>
			                			@if ($item->isGet == 0)
			                				<p style="color:blue;">Belum diambil</p>
			                			@elseif ($item->isGet == 1)
			                				<p style="color:green;">Sudah diambil</p>
			                			@else
											<p>-</p>
			                			@endif
			                			</center>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
			            </table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
		          <div class="modal-body">

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('coin') ? ' has-error' : '' }}">
		                    <label for="coin">Akumulasi Total Chip Bonus</label>
		                    <input id="coin" type="text" readonly="" value="{{ $totalChip->total }}" class="form-control" name="coin">
		                    @if ($errors->has('coin'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('coin') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                
		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin submit?')">
		        				<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Submit
		        			</button>
		        			
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("ambil-chip-bonus/store") }}/'+uuid+'/update');
    }
</script>

@endsection