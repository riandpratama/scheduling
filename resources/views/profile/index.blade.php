@extends('adminlte::page')

@section('title', 'Profile')

@section('content')

	<div class="col-md-4">
      <div class="box box-widget widget-user-2">
        <div class="widget-user-header bg-green">
          <div class="widget-user-image">
            <img class="img-circle" src="{{ 'storage/'.$user->userdetail->upload_foto }}" style="width: 80px; height: 80px;" alt="User Avatar">
          </div>
          <h5 class="widget-user-username" style="margin-left: 90px; font-size: 30px;">{{ $user->userdetail->nama_lengkap }}</h5>
        </div>
        <div class="box-footer no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">Kode Sponsor: <span class="pull-right "><b>{{ $user->sponsor->nama_sponsor }}</b></span></a></li>
            <li><a href="#">Username: <span class="pull-right ">{{ $user->username }}</span></a></li>
            <li><a href="#">Email: <span class="pull-right">{{ $user->email }}</span></a></li>
            <li><a href="#">Nama Lengkap: <span class="pull-right">{{ $user->userdetail->nama_lengkap }}</span></a></li>
            <li><a href="#">Tanggal Lahir: <span class="pull-right">{{ date('d/m/Y', strtotime($user->userdetail->tgl_lahir)) }}</span></a></li>
            <li><a href="#">Jenis Kelamin: <span class="pull-right">{{ $user->userdetail->jenis_kelamin }}</span></a></li>
            <li><a href="#">Alamat: <span class="pull-right">{{ $user->userdetail->alamat }}</span></a></li>
            <li><a href="#">No Handphone: <span class="pull-right">{{ $user->userdetail->no_hp }}</span></a></li>
            <li><a href="#">No KTP: <span class="pull-right">{{ $user->userdetail->no_ktp }}</span></a></li>
            <li><a href="#">No Rekening: <span class="pull-right">{{ $user->userdetail->no_rekening }}</span></a></li>
            <li><a href="#">Pemilik Rekening: <span class="pull-right">{{ $user->userdetail->nama_pemilik_rekening }}</span></a></li>
            <li><a href="#">Nama Bank: <span class="pull-right">{{ $user->userdetail->nama_bank }}</span></a></li>
            <li><a href="#">Nama Cabang Bank: <span class="pull-right">{{ $user->userdetail->nama_cabang }}</span></a></li>
            <li><a href="#">Swift Code Bank: <span class="pull-right">{{ $user->userdetail->swift_code }}</span></a></li>
          </ul>
        </div>
      </div>
      <button data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$user}}" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Ubah Informasi Akun</a>
      <!-- /.widget-user -->
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Profil</h4>
                </div>
                <div class="modal-body">
                    <form id="formEdit" method="POST" enctype="multipart/form-data">
                      {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input id="email" type="text" readonly="" class="form-control">
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username">Username</label>
                            <input id="username" type="text" readonly="" class="form-control">
                        </div>

                        <div class="form-group{{ $errors->has('nama_lengkap') ? ' has-error' : '' }}">
                            <label for="nama_lengkap">Nama Lengkap</label>
                            <input id="nama_lengkap" type="text" class="form-control" name="nama_lengkap">
                        </div>

                        <div class="form-group{{ $errors->has('tgl_lahir') ? ' has-error' : '' }}">
                            <label for="tgl_lahir">Tanggal Lahir</label>
                            <input id="tgl_lahir" type="date" class="form-control" name="tgl_lahir">
                        </div>

                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                            <label for="alamat">Alamat</label>
                            <input id="alamat" type="text" class="form-control" name="alamat">
                        </div>

                        <div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
                            <label for="no_hp">No Handphone</label>
                            <input id="no_hp" type="text" class="form-control" name="no_hp">
                        </div>

                        <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : '' }}">
                            <label for="no_ktp">No KTP</label>
                            <input id="no_ktp" type="text" class="form-control" name="no_ktp">
                        </div>

                        <div class="form-group{{ $errors->has('no_rekening') ? ' has-error' : '' }}">
                            <label for="no_rekening">No Rekening</label>
                            <input id="no_rekening" type="text" class="form-control" name="no_rekening">
                        </div>

                        <div class="form-group{{ $errors->has('nama_pemilik_rekening') ? ' has-error' : '' }}">
                            <label for="nama_pemilik_rekening">Nama Pemilik Rekening</label>
                            <input id="nama_pemilik_rekening" type="text" class="form-control" name="nama_pemilik_rekening">
                        </div>

                        <div class="form-group{{ $errors->has('nama_bank') ? ' has-error' : '' }}">
                            <label for="nama_bank">Nama Bank</label>
                            <input id="nama_bank" type="text" class="form-control" name="nama_bank">
                        </div>

                        <div class="form-group{{ $errors->has('nama_cabang') ? ' has-error' : '' }}">
                            <label for="nama_cabang">Nama Cabang Bank</label>
                            <input id="nama_cabang" type="text" class="form-control" name="nama_cabang">
                        </div>

                        <div class="form-group{{ $errors->has('swift_code') ? ' has-error' : '' }}">
                            <label for="swift_code">Swift Code</label>
                            <input id="swift_code" type="text" class="form-control" name="swift_code">
                        </div>
                        <div class="modal-footer">

                            <button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin merubah?')"><span class="glyphicon glyphicon-check" aria-hidden="true" ></span> Simpan</button>

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script>
    function showEdit(button){
        var item = $(button).data('item');
        console.log(item)
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("profile/update") }}/'+uuid+'/'+item.id);
        $('#formEdit .form-group #email').val(item.email);
        $('#formEdit .form-group #username').val(item.username);
        $('#formEdit .form-group #nama_lengkap').val(item.userdetail.nama_lengkap);
        $('#formEdit .form-group #tgl_lahir').val(item.userdetail.tgl_lahir);
        $('#formEdit .form-group #alamat').val(item.userdetail.alamat);
        $('#formEdit .form-group #no_hp').val(item.userdetail.no_hp);
        $('#formEdit .form-group #no_ktp').val(item.userdetail.no_ktp);
        $('#formEdit .form-group #nama_pemilik_rekening').val(item.userdetail.nama_pemilik_rekening);
        $('#formEdit .form-group #no_rekening').val(item.userdetail.no_rekening);
        $('#formEdit .form-group #nama_bank').val(item.userdetail.nama_bank);
        $('#formEdit .form-group #nama_cabang').val(item.userdetail.nama_cabang);
        $('#formEdit .form-group #swift_code').val(item.userdetail.swift_code);
    }
</script>
@endsection