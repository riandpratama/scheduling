<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Wallbroz - Syarat dan Ketentuan</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keyword" content="wallbroz, wallbroz.com, wallbrozdistrict, wallbroz district">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
    	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    </head>
    <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light site-navbar-target" id="ftco-navbar">
            <div class="container">
              <a class="navbar-brand" href="/">WallBroz District<span>.</span></a>
            </div>
          </nav>
        <section class="ftco-about img ftco-section" id="about-section">
            <div class="container">
                <div class="row d-flex no-gutters">
                    <div class="col-md-12 col-lg-12 pl-md-5">
                        <div class="row justify-content-start pb-3">
                            <div class="col-md-12 heading-section ftco-animate">
                                <h2 class="mb-4">Syarat dan Ketentuan</h2>
                                <div class="text-about">
                                    @include('syarat-ketentuan.index')
                                    @include('syarat-ketentuan.proses-registrasi')
                                    @include('syarat-ketentuan.tanggungjawab')
                                </div>

                                <p><a href="{{ route('register') }}" class="btn btn-primary py-3 px-4"><i class="fa fa-check"></i> Setuju</a></p>
                                <p><a href="{{ route('index') }}" class="btn btn-primary py-3 px-4"><i class="fa fa-close"></i> Tidak Setuju</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{ asset('assets/js/jquery-migrate-3.0.1.min.js')}}"></script>
        <script src="{{ asset('assets/js/popper.min.js')}}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('assets/js/jquery.easing.1.3.js')}}"></script>
        <script src="{{ asset('assets/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{ asset('assets/js/jquery.stellar.min.js')}}"></script>
        <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('assets/js/jquery.animateNumber.min.js')}}"></script>
        <script src="{{ asset('assets/js/scrollax.min.js')}}"></script>
        <script src="{{ asset('assets/js/google-map.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>
</html>