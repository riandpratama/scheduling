@extends('adminlte::page')

@section('title', 'Jual Chip (Exchanger)')

@section('content')
	<div class="box box-success">
        <div class="box-header with-border">
            <h5 class="box-title">Informasi</h5>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div>
                <p>Harga 1 Chip =  Rp<b> 20,000 </b></p>
            </div>
        </div>
    </div>
	<div class="box box-success">
        <div class="box-header with-border">
          	<div class="box-header with-border">
	          	<h3 class="box-title"> Jual Chip (Exchanger)</h3>
	          	{{-- @if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(3) )
		          	<br><br>
		          	<i style="color:red;">Akun anda kurang dari 3 bulan terhitung dari pendaftaran awal, maka ada potongan sebesar 10%</i>
		          	<br><br>
		          	<i style="color:red;">Total Biaya akan di akumulasikan saat submit.</i>
	        	@endif --}}
	        	<hr>
	        	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	                <p style="color:red;">
	                	Keterangan:<br>
	                	Akun anda kurang dari 6 bulan, maka 10 chip registrasi awal belum bisa di jual pada exchanger.
	                	<br>
	                	Jual chip ke exchanger (admin) di bawah 5 bulan dari Top Up maka kena potongan 20%
	                </p>
	            @endif
	        </div>
        </div>

        <form role="form" action="{{ route('ambil-coin.store', Auth::user()->id) }}" method="POST">
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
		        @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                 @endforeach
		        <div class="form-group">
	              	<label for="coin_saat_ini">Chip Saat Ini</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_saat_ini" placeholder="0" value="{{ ($coin->coin - 10 ) }}" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_saat_ini" placeholder="0" value="{{ $coin->coin }}" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="idr_saat_ini">IDR</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="text" class="form-control" id="idr_saat_ini" value="{{ number_format(($coin->coin - 10)*20000, 0) }}" readonly="">
	              	@else
	              	<input type="text" class="form-control" id="idr_saat_ini" value="{{ number_format($coin->coin*20000, 0) }}" readonly="">
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">Jumlah Maksimal Pengambilan Chip</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ substr(($coin->coin - 10), 0, -3) }}.00" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ substr($coin->coin, 0, -3) }}.00" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">IDR</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ number_format(substr(($coin->coin - 10), 0, -3)*20000, 0) }}" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ number_format(substr($coin->coin, 0, -3)*20000, 0) }}" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="username">Tujuan (Penerima)</label>
	              	<input type="text" class="form-control" id="username" value="administrator" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="coin">Chip <i style="color:red">*</i></label>
	                <input type="number" class="form-control chip" name="coin">
	            </div>
	            <div class="form-group">
	              	<label for="idr_pengambilan">Jumlah IDR Pengambilan</label>
	              	<div class="input-group">
	                	<span class="input-group-addon">Rp</span>
	                	<input type="text" class="form-control idr" name="biaya" readonly="">
              		</div>
	            </div>
	            <div class="form-group">
	              	<label for="password">Password <i style="color:red">*</i></label>
	              	<input type="password" class="form-control" name="password" id="password" >
	            </div>
	        </div>

	        <div class="box-footer">
	        	@if (!is_null($data))
	            <p style="color:red;">Menunggu pihak Administrator men konfirmasi penukaran anda.</p>
	            @else
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	            @endif
	       	</div>
        </form>
     </div>
@endsection

@section('js')
{{-- <script src="{{ asset('assets/js/inputmask/jquery.inputmask.bundle.min.js') }}" charset="utf-8"></script>
<script>
	$('.chip').inputmask({
	    alias: "decimal",
	    digits: 0,
	    repeat: 64,
	    rightAlign: false
	});
</script> --}}
<script type="text/javascript">

	var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};
	
	// $(".idr").keyup(function(){
	//     var idr = $(".idr").val();
	//     var now = "{{ \Carbon\Carbon::now() }}";
	//     var user = "{{ Auth::user()->created_at->addMonths(6) }}";

	//     if (now <= user) {
	//     	var batas = "{{ substr(($coin->coin-10), 0, -3)*20000 }}";
	//     } else {
	//     	var batas = "{{ substr($coin->coin, 0, -3)*20000 }}";
	//     }

	//     var jumlah = (parseInt(idr) / 20000);

	//     if(isNaN(jumlah)){
	//       	jumlah = 0;
	//     }

	//     if (parseInt(idr) > parseInt(batas) ) { // check if value changed
	//         alert('Maaf coin tidak cukup!');
	//     	$(".coin").val(0);
	//     } else {
	//     	$(".coin").val(jumlah);
	//     }

	// });
	
	$(".chip").keyup(function(){
	    var chip = $(".chip").val();
	    var now = "{{ \Carbon\Carbon::now() }}";
	    var user = "{{ Auth::user()->created_at->addMonths(6) }}";

	    if (now <= user) {
	    	var batas = "{{ ($coin->coin - 10) }}";
	    } else {
	    	var batas = "{{ $coin->coin }}";
	    }
	    
	    var jumlah = (parseInt(chip) * 20000);

	    if(isNaN(jumlah)){
	      	jumlah = 0;
	    }

	    if (parseInt(chip) > parseInt(batas) ) { // check if value changed
	        alert('Maaf chip tidak cukup!');
	    	$(".idr").val(0);
	    } else {
	    	$(".idr").val(format(jumlah));
	    }
	});
</script>
@endsection