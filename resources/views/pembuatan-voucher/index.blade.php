@extends('adminlte::page')

@section('title', 'Pembuatan Voucher')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Pembuatan Voucher</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
		        		<div  style="padding-bottom: 20px;">
			          		<button class="btn btn-primary" data-title="Edit" data-toggle="modal" data-target="#tambah">
			          			<span class="glyphicon glyphicon-plus"></span> Tambah Voucher
					        </button>
						</div>

		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Tanggal pembuatan</center>
				                        </th>
				                        <th>
				                            <center>Kode Voucher</center>
				                        </th>
				                        <th>
				                            <center>Kode Sponsor</center>
				                        </th>
				                        <th>
				                            <center>Status</center>
				                        </th>
				                        <th>
				                            <center>Kirim</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td><center>{{ $item->kode_vouchers }}</center></td>
				                		<td><center>{{ Auth::user()->sponsor->nama_sponsor }}</center></td>
				                		<td>
				                			<center>
				                			@if ($item->isActive == 0)
				                				<p style="color:green;">Sudah digunakan</p>
				                			@elseif ($item->isActive == 1)
				                				<p style="color:blue;">Belum digunakan</p>
				                			@else
												<p>-</p>
				                			@endif
				                			</center>
				                		</td>
				                		<td>
				                			<center>
				                				@if ($item->isActive == 1)
				                				<p data-placement="top" data-toggle="tooltip" title="Kirim">
				                					<button class="btn btn-success btn-xs" data-title="Kirim" data-toggle="modal" data-target="#kirim" onclick="showEdit(this);" data-item="{{$item}}"><i class="fab fa-whatsapp"></i>
				                					</button>
				                				</p>
				                				@else
				                					<span class="glyphicon glyphicon-ok"></span>
				                				@endif
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Pembuatan Voucher Baru</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
			        @if (Session::has('warning'))
			            <div class="alert alert-warning">
			                {{ Session::get('warning') }}
			            </div>
			        @endif
					@if (Auth::user()->ultracoin->coin < 20.00)
						<p style="color:red;">Maaf saldo chip anda kurang untuk melakuan pembuatan voucher baru, harap top up terlebih dahulu. </p>
					@endif
		          	<form id="formEdit" method="POST" enctype="multipart/form-data" action={{ route('pembuatan-voucher.store', Auth::user()->id) }}>
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                    <label for="email">Email</label>
		                    <input id="email" type="text" readonly="" class="form-control" value="{{ Auth::user()->email }}" name="email">
		                    @if ($errors->has('email'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('email') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('coin_saat_ini') ? ' has-error' : '' }}">
		                    <label for="coin_saat_ini">Chip Tersedia</label>
		                    <input id="coin_saat_ini" type="text" readonly="" class="form-control" value="{{ Auth::user()->ultracoin->coin }}" name="coin_saat_ini">
		                    @if ($errors->has('coin_saat_ini'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('coin_saat_ini') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('biaya') ? ' has-error' : '' }}">
		                    <label for="biaya">Nilai Voucher</label>
		                    <input id="biaya" type="text" readonly="" class="form-control" value="400.000" name="biaya">
		                    @if ($errors->has('biaya'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('biaya') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('coin') ? ' has-error' : '' }}">
		                    <label for="coin">Chip yang digunakan</label>
		                    <input id="coin" type="text" readonly="" class="form-control" value="20.00" name="coin">
		                    @if ($errors->has('coin'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('coin') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			              	<label for="password">Password <i style="color:red">*</i></label>
			              	<input type="password" class="form-control" name="password" id="password" >
			              	@if ($errors->has('password'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('password') }}</strong>
		                        </span>
		                    @endif
			            </div>
		                
		                <div class="modal-footer">
							@if (Auth::user()->ultracoin->coin > 20.00)
		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin submit?, *Saldo Chip anda akan terpotong sebesar 20.00')">
		        				<span class="glyphicon glyphicon-check" aria-hidden="true" ></span> Submit</button>
							@endif
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>

	<div class="modal fade" id="kirim" tabindex="-1" role="dialog" aria-labelledby="kirim" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
		        <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			    </div>
		        <div class="modal-body">
		        	<div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
	                    <label for="no_hp">Nomor WhatsApp</label>
	                    <input id="no_hp" type="number" class="form-control" name="no_hp">
	                    <b>Contoh</b>: <i style="color:red">628xxxxxxxxxx</i>
	                </div>
	                <span class="no_hp"></span>
	                <a target="_blank" href="" class="btn btn-success kirim_wa">Kirim</a>
	                {{-- 6285655340653 --}}
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

<script type="text/javascript">
    function showEdit(button){
        var item = $(button).data('item');

        var kodesponsor = "{{ Auth::user()->sponsor->nama_sponsor }}";

        var kodevoucher = item.kode_vouchers;

        $("#no_hp").keyup(function() {
        	var no = $("#no_hp").val();
        	var text = `Selamat bergabung, calon member baru. Gunakan kode sponspor *${kodesponsor}* dan kode voucher *${kodevoucher}* untuk regitrasi akun anda. Terima Kasih, Selamat Bergabung.`;
        	var url_wa = `https://api.whatsapp.com/send?phone=+${no}&text=${text}`;

        	$(".kirim_wa").attr("href", url_wa);
        });
        
        // console.log(no_hp)
        // $('.no_hp').append(no_hp);
    }
</script>

@endsection