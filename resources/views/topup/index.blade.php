@extends('adminlte::page')

@section('title', 'Top Up Saldo')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h5 class="box-title">Informasi</h5>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div>
                <p>Harga 1 Chip =  Rp<b> 25,000 </b></p>
            </div>
        </div>
    </div>
    
	<div class="box box-success">
        <form role="form" action="{{ route('topup.store', Auth::user()->id) }}" method="POST">
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
	            <div class="form-group">
	              	<label for="username">Akun user</label>
	              	<input type="text" class="form-control" id="username" value="{{ Auth::user()->username }}" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="username">Nama Rekening</label>
	              	<input type="text" class="form-control" id="username" value="Rossynta Widiyanti (BCA)" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="username">No Rekening</label>
	              	<input type="text" class="form-control" id="username" value="4700275944" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="coin">Chip</label>
	              	<input type="number" class="form-control coin" name="coin" id="coin" placeholder="0" >
	            </div>
	            <div class="form-group">
	              	<label for="coin">Fee Transaksi</label>
	              	<div class="input-group">
	                	<span class="input-group-addon">IDR</span>
	                	<input type="text" class="form-control idr" name="biaya" readonly="">
              		</div>
	            </div>
	        </div>

	        <div class="box-footer">
	            {{-- <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button> --}}
	            @if (!is_null($topup))
	            <p style="color:red;">Harap Konfirmasi Top up terlebih dahulu.</p>
	            @elseif (!is_null($topupverify))
	            <p style="color:red;">Menunggu Administrator mem verifikasi permintaan top up anda terlebih dahulu.</p>
	            @elseif (Auth::user()->ultracoin->ripening == 1)
	            <p style="color:red;">Akun Saldo Chip anda masih dalam proses ripening, beli chip dapat dilakukan besok hari.</p>
	            @else
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	            @endif
	       	</div>
        </form>
     </div>
@endsection

@section('js')
{{-- <script src="{{ asset('assets/js/inputmask/jquery.inputmask.bundle.min.js') }}" charset="utf-8"></script> --}}
<script type="text/javascript">

	var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(",");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return("Rp " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};
	
	$(".coin").keyup(function(){
	    var coin = $("#coin").val();
    	
	    var jumlah = (parseInt(coin) * 25000);
	    if(isNaN(jumlah)){
	      jumlah = 0;
	    }
	    $(".idr").val(format(jumlah));
	});
</script>
{{-- <script>
	$('.coin').inputmask({
	    alias: "decimal",
	    digits: 0,
	    repeat: 64,
	    rightAlign: false,
	    digitsOptional: false,
	    decimalProtect: true,
	    groupSeparator: ".",
	    placeholder: '0',
	    radixPoint: ",",
	    radixFocus: true,
	    autoGroup: true,
	    autoUnmask: false,
	    onBeforeMask: function(value, opts) {
	        return value;
	    },
	    removeMaskOnSubmit: true
	});
</script> --}}
@endsection