@extends('adminlte::page')

@section('title', 'Invoice')

@section('content')

    <section class="invoice" style="margin:0px;">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Wallbroz District.
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Dari:
                <address>
                    <strong>Admin Wallbroz.</strong><br>
                    Email: support@wallbroz.com
                </address>
            </div>
        
            <div class="col-sm-4 invoice-col">
                Kepada:
                <address>
                    <strong>{{ $data->pembeli->userdetail->nama_lengkap }}</strong><br>
                    {{ $data->provinsi }}, {{ $data->kabupaten }}<br>
                    {{ $data->alamat_kirim }}<br>
                    {{ $data->pembeli->userdetail->no_hp }}<br>
                    {{ $data->pembeli->email }}
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Faktur {{ $data->kode_pemesanan }}</b><br>
                <br>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Jumlah</th>
                            <th>Produk/Jasa</th>
                            <th>Rupiah</th>
                            <th>WbzStockroom</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data->pemesanandetail as $item)
                        <tr>
                            <td style="text-align: center;">{{ $item->jumlah_produk }}</td>
                            <td style="text-align: center;">{{ $item->produk['nama_produk'] }}</td>
                            <td style="text-align: center;">Rp.{{ number_format($item->harga_produk_rupiah,0) }}</td>
                            <td style="text-align: center;">{{ $item->harga_produk }}</td>
                            <td style="text-align: center;">{{ $item->harga_total_produk }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <p class="lead">Status Pemesanan:</p>
                @if ($data->status == 0)
                    <p class="badge" style="background-color: blue;">PROSES</p>
                @elseif ($data->status == 1)
                    <p class="badge" style="background-color: green;">KONFIRMASI</p>
                @elseif ($data->status == 2)
                    <p class="badge" style="background-color: red;">DITOLAK</p>
                @endif
                <p class="lead">Tanggal Konfirmasi: {{ date('d/m/Y', strtotime($data->tgl_konfirmasi)) }}</p>
            </div>
        
            <div class="col-xs-6">
                <p class="lead">Tanggal Pemesanan</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                            </tr>
                            <tr>
                                <th style="float: right;">Wbz {{ $data->harga_coin }}</th>
                            </tr>
                            <tr>
                                <th style="float: right;">Rp {{ number_format($data->harga_rupiah, 0) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{-- <div class="row no-print">
            <div class="col-xs-12">
                <a href="invoice-print.html" target="_blank" class="btn btn-default btn-block">
                    <i class="fa fa-print"></i> Download .pdf
                </a>
            </div>
        </div> --}}
    </section>
@endsection