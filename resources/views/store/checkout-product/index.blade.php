@extends('adminlte::page')

@section('title', 'Checkout Produk')

<link rel="stylesheet" href="{{ asset('assets/css/store.css') }}">

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger alert-hidden">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-body">
    <section class="checkout mt-5">
        <div class="container">
        	@if (!is_null($wbz))
        		@if($wbz->wbz <= number_format(\Cart::getTotal(), 2))
        			<a href="{{ route('cart.index') }}" class="btn btn-warning btn-sm"> <i class="fa fa-arrow-left"></i> Kembali</a>
		        	<hr>
		        	<p style="font-weight: bold;">Maaf Saldo WbzStockroom anda kurang.</p>
		        	<p style="font-weight: 300;">Harap melakukan penukaran saldo WbzStockroom terlebih dahulu atau mengurangi produk/jasa.</p>
		        @elseif ($wbz->status == 0)
		        	<a href="{{ route('cart.index') }}" class="btn btn-warning btn-sm"> <i class="fa fa-arrow-left"></i> Kembali</a>
		        	<hr>
		        	<p style="font-weight: bold;">Maaf Anda tidak dapat melanjutkan.</p>
		        	<p style="font-weight: 300;">Menunggu pihak Administrator memverifikasi pesanan anda terlebih dahulu.</p>
        		@else
		            <form method="POST" action="{{ route('checkout.store') }}">
		            	@csrf
			            <div class="row justify-content-between" style="margin-bottom: 100px;">
			                <div class="col-lg-6">
			                    <h4 class="mb-3" style=" font-weight: bold;"><u>Keterangan Pengirim Produk</u></h4>
			                    
			                    <div class="form-group">
			                        <label for="email">Email <i style="color:red;">*</i></label>
			                        <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" readonly="">
			                    </div>

			                    <div class="form-group">
			                        <label for="nama">Nama Lengkap <i style="color:red;">*</i></label>
			                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
			                        <input type="text" class="form-control" id="nama" name="nama" required="" value="{{ Auth::user()->userdetail->nama_lengkap }}">
			                    </div>
			                    
			                    <div class="form-group">
			                        <label for="nohp">Nomor Handphone <i style="color:red;">*</i></label>
			                        <input type="number" class="form-control" id="nohp" name="nohp" required="" value="{{ Auth::user()->userdetail->no_hp }}">
			                    </div>
			                    
			                    <div class="form-group">
			                    	<label for="">Provinsi <i style="color:red;">*</i></label>
			                        <select name="provinsi_id" id="provinsi" class="form-control" required="">
			                            <option selected="" disabled="">Pilih Provinsi </option>
			                            @foreach($data['rajaongkir']['results'] as $item)
			                            <option value="{{ $item['province_id'] }}" data-id="{{ $item['province'] }}">{{ $item['province'] }}</option>
			                            @endforeach
			                        </select>
			                        <input type="hidden" id="provinsiName" name="provinsi">
			                    </div>

			                    <div class="form-group">
			                        <label for="address2">Kota/Kabupaten <i style="color:red;">*</i></label>
			                        <select id="kabupaten" class="form-control" name="kabupaten_id"></select>
			                        <input type="hidden" id="kabupatenName" name="kabupaten">
			                    </div>

			                    <div class="form-group">
			                        <label for="address2">Alamat Lengkap (Sertakan Kecamatan/Desa) <i style="color:red;">*</i></label>
			                        <textarea type="text" class="form-control" name="alamat" id="alamat" required="" placeholder="Alamat"></textarea>
			                    </div>
			                </div>
			                <div class="col-lg-5">
			                <h5 class="card-title"><strong>Informasi Pemesanan</strong></h5>
			                    <div class="card">
			                    	<br>
			                        <div>
			                            @foreach (\Cart::getContent() as $item)
			                            <div class="col-md-12 col-sm-6 col-xs-12">
								          	<div class="info-box">
								            	<span class="info-box-icon">
								            		<img src="{{ config('app.endpoint_admin') }}/storages/images/500/{{ $item->attributes->image }}" width="100%">
								            	</span>
								            	<div class="info-box-content">
								              		<span class="info-box-text">{{ $item->name }}</span>
								              		<span class="info-box-number">{{ number_format($item->price,2) }}wbz x {{ $item->quantity }} = {{ number_format($item->price * $item->quantity,2) }}<small></small></span>
								            	</div>
								          	</div>
		        						</div>
						                <input type="hidden" name="produk_id[]" value="{{ $item->id }}" >
			                        	<input type="hidden" name="total_barang" value="{{ \Cart::getContent()->count() }}" >
			                        	<input type="hidden" name="total_pesan" value="{{ number_format(\Cart::getTotal(),2) }}" >
		                                <input type="hidden" name="harga_total_barang[]" value="{{ number_format($item->price * $item->quantity,2) }}" >
		                                <input type="hidden" name="jumlah_barang[]" value="{{ $item->quantity }}">
		                                <input type="hidden" name="harga_produk[]" value="{{ $item->price }}">
		                                <input type="hidden" name="harga_produk_rupiah[]" value="{{ $item->price * 20000 }}">
		                                <input type="hidden" name="harga_total_produk_rupiah[]" value="{{ ($item->price * 20000) * $item->quantity }}">
						                @endforeach
			                        </div>
			                    </div>
			                    <br>
			                    <div class="col-md-12 col-sm-12 col-xs-12">
						          	<div class="info-box">
						            	<div class="info-box-content">
						              		<p style="font-size: 16px;">Grand Total: </p> <br>
						              		<b style="font-size: 16px;"> {{ number_format(\Cart::getTotal(),2) }} WbzStockroom</b>
						            	</div>
						          	</div>
								</div>
			                    <div class="col-md-12 col-sm-6 col-xs-12">
								    <div class="info-box">
			                       		<a href="{{ route('cart.index') }}" class="btn btn-warning btn-sm"><i class="fa fa-angle-left"></i> Kembali</a>
			                       		<button type="submit" onclick="return confirm('Lanjutkan pemesanan?')" class="btn btn-success btn-sm pull-right"> Proses <i class="fa fa-angle-right"></i></button>
			                    	</div>
			                    </div>
			                </div>
			            </div>
		        	</form>
	        	@endif
        	@else
        	<a href="{{ route('cart.index') }}" class="btn btn-warning btn-sm"> <i class="fa fa-arrow-left"></i> Kembali</a>
        	<hr>
        	<p style="font-weight: bold;">Maaf Saldo WbzStockroom anda belum aktif.</p>
        	<p style="font-weight: 300;">Harap melakukan penukaran saldo WbzStockroom terlebih dahulu.</p>
        	@endif
    	</div>
	</section>
</div>
@endsection

@section('js')
<script type="text/javascript">
    var baseUrl = '{{ url('/') }}';

    $(document).ready(function(){
        $('#provinsi').change(function(){
 
            var prov = $('#provinsi').val();
            var provName = $('#provinsi').find(':selected').attr('data-id')
            
            $('#provinsiName').attr("value", provName);
            
            $.ajax({
                type : 'GET',
                url : baseUrl + '/cek-kabupaten',
                data :  'prov_id=' + prov,
                    success: function (data) {
                    
                    $("#kabupaten").html(data);

                    var kabName = $('#kabupaten').find(':selected').attr('data-id')
                    
                    $('#kabupatenName').attr("value", kabName);
                    console.log(kabName)

                    $('#kabupaten').change(function(){
                        var kabName = $('#kabupaten').find(':selected').attr('data-id')
                        $('#kabupatenName').attr("value", kabName);
                        console.log(kabName)
                    });

                }
            });
        });
    });
</script>
@endsection