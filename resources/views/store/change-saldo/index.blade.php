@extends('adminlte::page')

@section('title', 'Tukar Saldo Chip WBZ ke WBZ Stockroom')

@section('content')

	<div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-green">
            	<span class="info-box-icon"><i class="fa fa-credit-card"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">SALDO </span>
	              <span class="info-box-number">WBZ Stockroom</span>

	              <div class="progress">
	                <div class="progress-bar" style="width: 100%"></div>
	              </div>
	                  <span class="progress-description">
	                    @if (is_null($wbz))
	                    	Belum Aktif
	                    @else
	                    	{{ $wbz->wbz }}
	                    @endif
	                  </span>
	            </div>
          	</div>
        </div>
    </div>

	<div class="box box-success">
        <div class="box-header with-border">
          	<div class="box-header with-border">
	          	<h3 class="box-title"> Tukar saldo wbz stockroom</h3>
	        	<hr>
	        	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	                <p style="color:red;">
	                	Keterangan:<br>
	                	Akun anda kurang dari 6 bulan, maka 10 chip registrasi awal belum bisa di tukar menjadi saldo wbz stockroom.
	                </p>
	            @endif
	        </div>
        </div>

        <form role="form" action="{{ route('wbz.store', Auth::user()->id) }}" method="POST">
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
		        @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                 @endforeach
		        <div class="form-group">
	              	<label for="coin_saat_ini">Chip Wbz Anda</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_saat_ini" placeholder="0" value="{{ ($coin->coin - 10 ) }}" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_saat_ini" placeholder="0" value="{{ $coin->coin }}" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="idr_saat_ini">IDR Chip Wbz Anda</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="text" class="form-control" id="idr_saat_ini" value="{{ number_format(($coin->coin - 10)*20000, 0) }}" readonly="">
	              	@else
	              	<input type="text" class="form-control" id="idr_saat_ini" value="{{ number_format($coin->coin*20000, 0) }}" readonly="">
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">Jumlah Maksimal Penukaran Chip Wbz</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ substr(($coin->coin - 10), 0, -3) }}.00" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ substr($coin->coin, 0, -3) }}.00" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">IDR</label>
	              	@if (\Carbon\Carbon::now() <= Auth::user()->created_at->addMonths(6))
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ number_format(substr(($coin->coin - 10), 0, -3)*20000, 0) }}" readonly="" >
	              	@else
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ number_format(substr($coin->coin, 0, -3)*20000, 0) }}" readonly="" >
	              	@endif
	            </div>
	            <div class="form-group">
	              	<label for="idr_pengambilan">Jumlah IDR Pengambilan <i style="color:red">*</i></label>
	              	<div class="input-group">
	                	<span class="input-group-addon">Rp</span>
	                	<input type="text" class="form-control idr" name="biaya" autocomplete="off">
              		</div>
	                <i style="color:red;">*Jumlah minimum penukaran saldo 20.000 = 1 chip wbz</i>
	            </div>
	            <div class="form-group">
	              	<label for="wbz">Wbz Stockroom </label>
	                <input type="number" class="form-control wbz" name="wbz" readonly="">
	            </div>
	            
	            <div class="form-group">
	              	<label for="password">Password <i style="color:red">*</i></label>
	              	<input type="password" class="form-control" name="password" id="password" >
	            </div>
	        </div>

	        <div class="box-footer">
	            @if (is_null($wbzRiwayat))
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	            @else
	            <p style="color:green;">Menunggu admin verifikasi penukaran anda.</p>
	            @endif
	       	</div>
        </form>
     </div>
@endsection

@section('js')

<script type="text/javascript">

	var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

	$(".idr").keyup(function(){
	    var idr = $(".idr").val();
	    var now = "{{ \Carbon\Carbon::now() }}";
	    var user = "{{ Auth::user()->created_at->addMonths(6) }}";

	    if (now <= user) {
	    	var batas = "{{ substr(($coin->coin-10), 0, -3)*20000 }}";
	    } else {
	    	var batas = "{{ substr($coin->coin, 0, -3)*20000 }}";
	    }

	    var jumlah = (parseInt(idr) / 20000);

	    if(isNaN(jumlah)){
	      	jumlah = 0;
	    }

	    if (parseInt(idr) > parseInt(batas) ) { // check if value changed
	        alert('Maaf chip wbz tidak cukup!');
	    	$(".wbz").val(0);
	    } else {
	    	$(".wbz").val(jumlah);
	    }

	});
</script>

@endsection