@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
    
    <section class="cart-header mt-5">
        <div class="container text-center">
            <div class="row">
                <div class="col">
                    <h3>Keranjang Anda</h3>
                    <br>
                </div>
            </div>
        </div>
    </section>

    {{-- <div class="container"> --}}
        @if (Session::has('success_message'))
        <div class="alert alert-success" role="alert">
            <strong></strong>{{ Session::get('success_message') }}
        </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-shopping-cart"></i> Keranjang Belanja</div>
            <div class="panel-body">
                @if (count(\Cart::getContent()) > 0)
                <div class="box-body table-responsive no-padding">
                    <table id="cart" class="table table-bordered" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="width:18%; text-align: center;">Gambar Produk</th>
                                <th style="width:30%; text-align: center;">Nama Produk</th>
                                <th style="width:10%">Harga</th>
                                <th style="width:8%">Stok</th>
                                <th style="width:22%" class="text-center">Subtotal</th>
                                <th style="width:10%">Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- {{ dd(\Cart::getContent()) }} --}}
                            @foreach (\Cart::getContent() as $item)
                            <tr>
                                <td data-th="Gambar">
                                    <div class="col-sm-3 ">
                                        <img src="{{ config('app.endpoint_admin') }}/storages/images/500/{{ $item->attributes->image }}" alt="{{ $item->nama_produk }}" class="img-responsive">
                                    </div>
                                </td>
                                <td data-th="Product" style="text-align: center;">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <p class="nomargin" style="margin-left: 30px;">{{ $item->name }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td data-th="Price">{{ number_format($item->price,2) }}</td>
                                <td data-th="Quantity">
                                    <select class="quantity"  data-id="{{ $item->id }}">
                                        @for ($i = 1; $i < $item->attributes->stok + 1 ; $i++)
                                            <option {{ $item->quantity == $i ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td data-th="Subtotal" class="text-center">{{ number_format($item->price * $item->quantity,2) }}</td>
                                <td class="actions" data-th="">

                                    <form action="{{ route('cart.destroy', $item->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus barang dari keranjang?')" ><i class="fa fa-trash"></i></button> 
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                {{-- <td><a href="{{ route('store.list') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td> --}}
                                
                                <td colspan="4" class="text-center"><strong>Total : </strong></td>
                                <td class="text-center"><strong> {{ number_format(\Cart::getTotal(),2) }}</strong></td>
                                <td>
                                    {{-- <a href="{{ route('frontend.checkout') }}" class="btn btn-success btn-block">Proses <i class="fa fa-angle-right"></i>
                                    </a> --}}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @else
                Tidak Ada Produk
                @endif
            </div>
        </div>
        <a href="{{ route('store.list') }}" class="btn btn-warning btn-sm"><i class="fa fa-angle-left"></i> Continue Shopping</a>
        @if (count(\Cart::getContent()) > 0)
        <a href="{{ route('checkout.index') }}" class="btn btn-success btn-sm pull-right"> Checkout <i class="fa fa-angle-right"></i></a>
        @endif
    {{-- </div> --}}

@endsection

@section('js')
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        (function(){
            const classname = document.querySelectorAll('.quantity')

            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {
                    const id = element.getAttribute('data-id')
                    axios.patch(`/store-penjualan/${id}`, {
                        quantity: this.value
                    })
                    .then(function (response) {
                        // console.log(response);
                        window.location.href = '{{ route('cart.index') }}'
                    })
                    .catch(function (error) {
                        // console.log(error);
                        window.location.href = '{{ route('cart.index') }}'
                    });
                })
            })
        })();
    </script>

@endsection