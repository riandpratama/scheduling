@extends('adminlte::page')

@section('title', 'Kirim WbzStockroom ke Chip WBZ')

@section('content')

	<div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-green">
            	<span class="info-box-icon"><i class="fa fa-credit-card"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">SALDO </span>
	              <span class="info-box-number">Chip WbzStockroom</span>

	              <div class="progress">
	                <div class="progress-bar" style="width: 100%"></div>
	              </div>
	                  <span class="progress-description">
	                    @if (is_null($wbz))
	                    	Belum Aktif
	                    @else
	                    	{{ $wbz->wbz }}
	                    @endif
	                  </span>
	            </div>
          	</div>
        </div>
    </div>

	<div class="box box-success">
		@if(is_null($wbz))
		<div class="box-header with-border">
			<p>Saldo Wbzstockroom anda belum Aktif.</p>
		</div>
    	@else

        <form role="form" action="{{ route('wbz.kirim.store', Auth::user()->id) }}" method="POST">
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
		        @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                 @endforeach
		        <div class="form-group">
	              	<label for="coin_saat_ini">Wbz Stockroom Anda</label>
	              	<input type="numeric" class="form-control" name="coin_saat_ini" placeholder="0" value="{{ $wbz->wbz }}" readonly="" >
	            </div>
	            <div class="form-group">
	              	<label for="idr_saat_ini">IDR Wbz Stockroom Anda</label>
	              	<input type="text" class="form-control" id="idr_saat_ini" value="{{ number_format($wbz->wbz*20000, 0) }}" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">Jumlah Maksimal Penukaran Chip Wbz</label>
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ substr($wbz->wbz, 0, -3) }}.00" readonly="" >
	            </div>
	            <div class="form-group">
	              	<label for="coin_max">IDR</label>
	              	<input type="numeric" class="form-control" name="coin_max" placeholder="0" value="{{ number_format(substr($wbz->wbz, 0, -3)*20000, 0) }}" readonly="" >
	            </div>
	            <div class="form-group">
	              	<label for="idr_pengambilan">Jumlah IDR Pengiriman <i style="color:red">*</i></label>
	              	<div class="input-group">
	                	<span class="input-group-addon">Rp</span>
	                	<input type="text" class="form-control idr" name="biaya" autocomplete="off">
              		</div>
	            </div>
	            <div class="form-group">
	              	<label for="wbz">Chip Wbz </label>
	                <input type="number" class="form-control wbz" name="wbz" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="deskripsi">Deskripsi </label>
	                <textarea type="deskripsi" class="form-control" name="deskripsi"></textarea>
	            </div>
	            <div class="form-group">
	              	<label for="password">Password <i style="color:red">*</i></label>
	              	<input type="password" class="form-control" name="password" id="password" >
	            </div>
	        </div>

	        <div class="box-footer">
	        	@if (is_null($wbzRiwayat))
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	            @else
	            <p style="color:green;">Menunggu admin verifikasi pengiriman anda.</p>
	            @endif
	       	</div>
        </form>
        @endif
     </div>
@endsection

@section('js')

<script type="text/javascript">

	var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

	$(".idr").keyup(function(){
	    var idr = $(".idr").val();
	    
	    var batas = "{{ substr($wbz['wbz'], 0, -3)*20000 }}";

	    var jumlah = (parseInt(idr) / 20000);

	    if(isNaN(jumlah)){
	      	jumlah = 0;
	    }

	    if (parseInt(idr) > parseInt(batas) ) { // check if value changed
	        alert('Maaf wbzstockroom tidak cukup!');
	    	$(".wbz").val(0);
	    } else {
	    	$(".wbz").val(jumlah);
	    }

	});
</script>

@endsection