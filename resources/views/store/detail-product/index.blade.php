@extends('adminlte::page')

@section('title', 'List Produk')
<link rel="stylesheet" href="{{ asset('assets/css/store.css') }}">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<style>
    .swiper-container {
        width: 100%;
    }
    .swiper-slide {
        background-size: cover;
        background-position: 50%;
        min-height: 20vh;
        
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }
    .swiper-pagination-bullet {
        width: 20px;
        height: 20px;
        text-align: center;
        line-height: 20px;
        font-size: 12px;
        color: #000;
        opacity: 1;
        background: rgba(0, 0, 0, 0.2);
    }
    .swiper-pagination-bullet-active {
        color: #fff;
        background: #007aff;
    }
</style>
@section('content')
<div class="row">
	<div class="col-md-12">
      	<div class="box box-success">
        	<div class="box-header with-border">
          		<h3 class="box-title">Toko Wallbrozz District</h3>
          		<div class="box-tools pull-right">
            		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          		</div>
        	</div>
            <div class="box-body" style="">
              	<div>
              		<a href="{{ route('cart.index') }}" class="btn btn-primary">
              			<i class="fa fa-shopping-cart"></i>
              			Total Keranjang : 
              			<span class="badge badge-warning">{{ count(\Cart::getContent()) }}</span>
              		</a>
              	</div>
            </div>
      	</div>
    </div>
    
    <section class="single-item">
        <div class="container">
            <a href="{{ route('store.list') }}" class="btn btn-warning btn-sm"><i class="fa fa-angle-left"></i> Continue Shopping</a>
            <div class="row">
                @if($data == NULL)
                <div class="col-sm-7 col-md-6 col-lg-5">
                    <p>Maaf Tidak Ada Produk.</p>
                </div>
                @else
                <div class="col-sm-7 col-md-6 col-lg-12">
                    <div class="box box-success box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><p style="color:black;">Gambar Produk</p></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <figure class="figure">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        @foreach($data->produkimage as $item)
                                        <div class="swiper-slide">
                                            <img src="{{ config('app.endpoint_admin') }}/storages/images/detail/500/{{ $item->image_path }}">
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-sm-7 col-md-6 col-lg-12">
                    <div class="box box-success box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><p style="color:black;">Detail Produk</p></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="box-footer no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Nama Produk: <span class="pull-right "><b>{{ $data->nama_produk }}</b></span></a></li>
                                    <li><a href="#">Harga: <span class="pull-right "><b>{{ $data->harga_coin }} WBZ</b></span></a></li>
                                    <li><a href="#">Stok: <span class="pull-right "><b>{{ $data->stok }}</b></span></a></li>
                                    <li><a href="#">Deskripsi: <span class="pull-right "></span></a></li>
                                </ul>
                            </div>
                            <div class="description-block">
                                <p class="text-center">{!! $data->deskripsi !!}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-7 col-md-6 col-lg-12">
                    <div class="box box-success box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><p style="color:black;">Detail Penjual</p></h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-lg-2 col-xs-4">
                                <img src="{{ asset('assets/images/person.png') }}" class="img-responsive" style="width: 50%">
                            </div>
                            <div class="col-lg-2 col-xs-6">
                                <p class="mb-0" style="font-weight: bold; color: #707070;">{{ $data->user->username }}</p>
                                <p class="mb-0" style="font-weight: bold; color: #707070;">Member Sejak: {{ date('d/m/Y', strtotime($data->user->created_at)) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>                
            <a href="{{ route('message', $data->user->id) }}" class="btn btn-success" ><i class="fa fa-comment"></i> <b>Chat Penjual</b></a>
            <form action="{{ route('cart.store') }}" method="POST" style="display: inline">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $data->id }}">
                <input type="hidden" name="name" value="{{ $data->nama_produk }}">
                <input type="hidden" name="price" value="{{ $data->harga_coin }}">
                <input type="hidden" name="quantity" value="1">
                <input type="hidden" name="stok" value="{{ $data->stok }}">
                <input type="hidden" name="image" value="{{ $data->image_sampul }}">
                
                <button type="submit" class="btn btn-primary btn-block" action={{ route('cart.store') }}>
                    <i class="fa fa-shopping-cart"></i> <b>Masukkan Keranjang</b>
                </button>
            </form>
        </div>
    </section>
</div>
@endsection

@section('js')


<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script>
	var Swipes = new Swiper('.swiper-container', {
    loop: true,
    autoHeight: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
    },
});
</script>
@endsection