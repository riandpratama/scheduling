@extends('adminlte::page')

@section('title', 'Riwayat Kirim WbzStockroom')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Kirim WbzStockroom</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body">
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
		                            	<center>No.</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Email</center>
			                        </th>
			                        <th>
			                            <center>Wbz</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Deskripsi</center>
			                        </th>
			                        <th>
			                            <center>Konfirmasi</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($data as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td><center>{{ $item->user->email }}</center></td>
			                		<td><center>{{ number_format($item->wbz, 2) }}</center></td>
			                		<td><center>Rp.{{ number_format($item->rupiah, 0, ',', '.') }}</center></td>
			                		<td><center>{{ $item->deskripsi }}</center></td>
			                		<td>
			                			<center>
			                				@if ($item->status == 0)
			                				<span style="color:blue;">Proses</span>
			                				@else
			                					<span style="color:green;"> Terkonfirmasi</span>
			                				@endif
			                			</center>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
			            </table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("konfirmasi-topup/store") }}/'+uuid+'/'+item.id+'/update');
        $('#formEdit .form-group #faktur').val(item.faktur);
    }
</script>
@endsection