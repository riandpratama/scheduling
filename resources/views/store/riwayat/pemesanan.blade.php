@extends('adminlte::page')

@section('title', 'Riwayat Pemesanan')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Pemesanan</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body">
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
		                            	<center>No.</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Penjual</center>
			                        </th>
			                        <th>
			                            <center>WbzStockroom</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Status</center>
			                        </th>
			                        <th>
			                            <center>Detail</center>
			                        </th>
			                        <th>
			                            <center>Invoice</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($data as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td><center>{{ $item->penjual->username }}</center></td>
			                		<td><center>{{ number_format($item->harga_coin, 2) }}</center></td>
			                		<td><center>Rp.{{ number_format($item->harga_rupiah, 0, ',', '.') }}</center></td>
			                		<td>
			                			<center>
			                				@if ($item->status == 0)
			                				<span style="color:blue;">Proses</span>
			                				@else
			                					<span style="color:green;"> Terkonfirmasi</span>
			                				@endif
			                			</center>
			                		</td>
			                		<td>
			                			<button 
							          		class="btn btn-success btn-xs" 
							          		data-title="Edit" data-toggle="modal" 
							          		data-target="#edit" 
							          		data-item="{{ $item }}"
							          		onclick="showEdit(this);" >
							          		<i class="fa fa-eye"></i>
							          	</button>
			                		</td>
			                		<td>
			                			<a href="{{ route('checkout.invoice', $item->id) }}" class="btn btn-xs btn-info"><i class="fa fa-credit-card"></i></a>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
			            </table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
		        <div class="modal-body">

	                <div class="form-group{{ $errors->has('kode_pemesanan') ? ' has-error' : '' }}">
	                    <label for="kode_pemesanan">Kode Pemesanan</label>
	                    <input id="kode_pemesanan" type="text" readonly="" class="form-control" name="kode_pemesanan">
	                </div>

	                <div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
	                    <label for="provinsi">Provinsi</label>
	                    <input id="provinsi" type="text" readonly="" class="form-control" disabled="">
	                </div>

	                <div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
	                    <label for="kabupaten">Kabupaten</label>
	                    <input id="kabupaten" type="text" readonly="" class="form-control" disabled="">
	                </div>

	                <div class="form-group{{ $errors->has('alamat_kirim') ? ' has-error' : '' }}">
	                    <label for="alamat_kirim">Alamat</label>
	                    <input id="alamat_kirim" type="text" readonly="" class="form-control" disabled="">
	                </div>

	                <div class="form-group{{ $errors->has('total_pesan') ? ' has-error' : '' }}">
	                    <label for="total_pesan">Total Pesanan</label>
	                    <input id="total_pesan" type="text" readonly="" class="form-control" disabled="">
	                </div>

	                <div class="form-group{{ $errors->has('harga_coin') ? ' has-error' : '' }}">
	                    <label for="harga_coin">Total Wbzstockroom</label>
	                    <input id="harga_coin" type="text" readonly="" class="form-control" disabled="">
	                </div>

	                <div class="form-group">
	                    <label for="produk">Nama Produk/Jasa</label>
	                    <p id="produk"></p> <span id="harga"></span>
	                </div>
	                
	                <div class="modal-footer">
	        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Close</button>
	      	  		</div>  
		      	  	
		      	</div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        console.log(item);
        var uuid = "{{ Auth::user()->id }}";
        // $('form#formEdit').attr('action','{{ url("konfirmasi-topup/store") }}/'+uuid+'/'+item.id+'/update');
        $('#kode_pemesanan').val(item.kode_pemesanan);
        $('#provinsi').val(item.provinsi);
        $('#kabupaten').val(item.kabupaten);
        $('#alamat_kirim').val(item.alamat_kirim);
        $('#total_pesan').val(item.total_pesan);
        $('#harga_coin').val(item.harga_coin);
        $('#produk').html('');
        for(i=0; i < item.pemesanandetail.length; i++){
        	console.log(item.pemesanandetail[i]);
        	$('#produk').append('<p>'+ item.pemesanandetail[i].produk.nama_produk + '&nbsp;&nbsp;&nbsp;' + item.pemesanandetail[i].jumlah_produk + '&nbsp;&nbsp;&nbsp;' + item.pemesanandetail[i].harga_total_produk + '<p>');
        }
        // $('#produk').append()
    }
</script>
@endsection