@extends('adminlte::page')

@section('title', 'List Produk')
<link rel="stylesheet" href="{{ asset('assets/css/store.css') }}">
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
                <form action="{{ route('store.search') }}" method="get">
				    <div class="input-group input-group-sm">
    	                <input type="text" class="form-control" name="search" placeholder="Ketikkan produk/jasa yang ingin dicari ...">
                        <span class="input-group-btn">
                          	<button type="submit" class="btn btn-success btn-flat"><i class="fa fa-search"></i></button>
                        </span>
	               </div>
                </form>
			</div>
		</div>
		<div class="col-md-12">
          	<div class="box box-success">
            	<div class="box-header with-border">
              		<h3 class="box-title">Toko Wallbrozz District</h3>
              		<div class="box-tools pull-right">
                		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              		</div>
            	</div>
	            <div class="box-body" style="">
	              	<div>
	              		<a href="{{ route('cart.index') }}" class="btn btn-primary">
	              			<i class="fa fa-shopping-cart"></i>
	              			Total Keranjang : 
	              			<span class="badge badge-warning">{{ count(\Cart::getContent()) }}</span>
	              		</a>
                        <a href="{{ route('message', '01537fd0-4698-4fc6-8429-51b1e58bee94') }}" class="btn btn-info pull-right">
                            <i class="fa fa-comment"></i>
                            Messages
                            @if (!is_null($message))
                                @if ($message->to == Auth::id() && $message->is_read == 0)
                                    <span class="badge badge-warning" style="background-color: red; color: white;">NEW!</span>
                                @else
                                    0
                                @endif
                            @endif
                        </a>
	              	</div>
	            </div>
          	</div>
        </div>
	    <br>
		
        @foreach ($data as $item)
		<div class="col-lg-3 col-xs-6">
        	<div class="box inner">
          		<figure class="figure">
                    <div class="figure-img">
                        <img src="{{ config('app.endpoint_admin') }}/storages/images/500/{{ $item->image_sampul }}" alt="prdouk1" class="img-thumbnail">
                        <a href="{{ route('store.show', $item->slug) }}" class="d-flex justify-content-center">
                            <img src="{{ asset('assets/images/search.png') }}"
                                style="width: 20%; display: block; margin: auto; margin-top: 50%;">
                        </a>
                    </div>
                    <figcaption class="figure-caption text-center">
                        <h4>{{ $item->nama_produk }}</h4>
                        <p>{{ $item->harga_coin }} WBZ</p>
                        <a href="{{ route('store.show', $item->slug) }}" class="btn btn-success btn-block"><i class="fa fa-search"></i> Detail Produk</a>
                    </figcaption>
                </figure>
        	</div>
	    </div>
        @endforeach
		
	</div>
@endsection

@section('js')

@endsection